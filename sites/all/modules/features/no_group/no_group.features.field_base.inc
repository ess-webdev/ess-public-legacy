<?php
/**
 * @file
 * no_group.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function no_group_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'og_user_group_ref'.
  $field_bases['og_user_group_ref'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'og_user_group_ref',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'og',
      'handler_settings' => array(
        'behaviors' => array(
          'og_behavior' => array(
            'status' => TRUE,
          ),
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'hide_secondary_field' => 1,
        'membership_type' => 'og_membership_type_default',
        'primary_field' => 0,
        'reference_type' => 'all_groups',
        'sort' => array(
          'direction' => 'ASC',
          'field' => 'field_no_section:target_id',
          'property' => 'nid',
          'type' => 'none',
        ),
        'target_bundles' => array(
          'no_section' => 'no_section',
        ),
      ),
      'handler_submit' => 'Change handler',
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
