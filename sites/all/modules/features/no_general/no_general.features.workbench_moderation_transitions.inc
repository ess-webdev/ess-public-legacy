<?php
/**
 * @file
 * no_general.features.workbench_moderation_transitions.inc
 */

/**
 * Implements hook_workbench_moderation_export_transitions().
 */
function no_general_workbench_moderation_export_transitions() {
  $items = array(
    'published:draft' => array(
      'from_name' => 'published',
      'to_name' => 'draft',
      'name' => 'New Draft',
    ),
  );
  return $items;
}
