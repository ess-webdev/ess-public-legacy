<?php
/**
 * @file
 * no_roles_and_permissions.features.og_features_permission.inc
 */

/**
 * Implements hook_og_features_default_permissions().
 */
function no_roles_and_permissions_og_features_default_permissions() {
  $permissions = array();

  // Exported og permission: 'node:no_section:add user'
  $permissions['node:no_section:add user'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:no_section:administer group'
  $permissions['node:no_section:administer group'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:no_section:approve and deny subscription'
  $permissions['node:no_section:approve and deny subscription'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:no_section:create no_article content'
  $permissions['node:no_section:create no_article content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:create no_event content'
  $permissions['node:no_section:create no_event content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:create no_page content'
  $permissions['node:no_section:create no_page content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:no_section:delete any no_article content'
  $permissions['node:no_section:delete any no_article content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:delete any no_event content'
  $permissions['node:no_section:delete any no_event content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:delete any no_page content'
  $permissions['node:no_section:delete any no_page content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:delete own no_article content'
  $permissions['node:no_section:delete own no_article content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:delete own no_event content'
  $permissions['node:no_section:delete own no_event content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:delete own no_page content'
  $permissions['node:no_section:delete own no_page content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:no_section:manage members'
  $permissions['node:no_section:manage members'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:manage permissions'
  $permissions['node:no_section:manage permissions'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:manage roles'
  $permissions['node:no_section:manage roles'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:publish any content'
  $permissions['node:no_section:publish any content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:publish any no_page content'
  $permissions['node:no_section:publish any no_page content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:publish editable content'
  $permissions['node:no_section:publish editable content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:no_section:publish editable no_page content'
  $permissions['node:no_section:publish editable no_page content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:publish own no_page content'
  $permissions['node:no_section:publish own no_page content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:subscribe'
  $permissions['node:no_section:subscribe'] = array(
    'roles' => array(
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:no_section:subscribe without approval'
  $permissions['node:no_section:subscribe without approval'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:unpublish any content'
  $permissions['node:no_section:unpublish any content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:unpublish any no_page content'
  $permissions['node:no_section:unpublish any no_page content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:unpublish editable content'
  $permissions['node:no_section:unpublish editable content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:no_section:unpublish editable no_page content'
  $permissions['node:no_section:unpublish editable no_page content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:unpublish own no_page content'
  $permissions['node:no_section:unpublish own no_page content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:unsubscribe'
  $permissions['node:no_section:unsubscribe'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:update any no_article content'
  $permissions['node:no_section:update any no_article content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:update any no_event content'
  $permissions['node:no_section:update any no_event content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:update any no_page content'
  $permissions['node:no_section:update any no_page content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:no_section:update group'
  $permissions['node:no_section:update group'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:no_section:update own no_article content'
  $permissions['node:no_section:update own no_article content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:update own no_event content'
  $permissions['node:no_section:update own no_event content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:no_section:update own no_page content'
  $permissions['node:no_section:update own no_page content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  return $permissions;
}
