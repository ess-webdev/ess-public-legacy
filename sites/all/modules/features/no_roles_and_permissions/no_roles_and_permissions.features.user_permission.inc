<?php
/**
 * @file
 * no_roles_and_permissions.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function no_roles_and_permissions_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access administration menu'.
  $permissions['access administration menu'] = array(
    'name' => 'access administration menu',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: 'access content'.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
      'Site administrator' => 'Site administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'access content overview'.
  $permissions['access content overview'] = array(
    'name' => 'access content overview',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'add media from remote sources'.
  $permissions['add media from remote sources'] = array(
    'name' => 'add media from remote sources',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'media_internet',
  );

  // Exported permission: 'administer files'.
  $permissions['administer files'] = array(
    'name' => 'administer files',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'file_entity',
  );

  // Exported permission: 'administer group'.
  $permissions['administer group'] = array(
    'name' => 'administer group',
    'roles' => array(
      'Developer' => 'Developer',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'og',
  );

  // Exported permission: 'administer menu'.
  $permissions['administer menu'] = array(
    'name' => 'administer menu',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'menu',
  );

  // Exported permission: 'administer nodes'.
  $permissions['administer nodes'] = array(
    'name' => 'administer nodes',
    'roles' => array(
      'Developer' => 'Developer',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'administer permissions'.
  $permissions['administer permissions'] = array(
    'name' => 'administer permissions',
    'roles' => array(
      'Developer' => 'Developer',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'user',
  );

  // Exported permission: 'administer taxonomy'.
  $permissions['administer taxonomy'] = array(
    'name' => 'administer taxonomy',
    'roles' => array(
      'Developer' => 'Developer',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'administer users'.
  $permissions['administer users'] = array(
    'name' => 'administer users',
    'roles' => array(
      'Developer' => 'Developer',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'user',
  );

  // Exported permission: 'bypass node access'.
  $permissions['bypass node access'] = array(
    'name' => 'bypass node access',
    'roles' => array(
      'Developer' => 'Developer',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create no_page content'.
  $permissions['create no_page content'] = array(
    'name' => 'create no_page content',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create no_promo content'.
  $permissions['create no_promo content'] = array(
    'name' => 'create no_promo content',
    'roles' => array(
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own no_page content'.
  $permissions['delete own no_page content'] = array(
    'name' => 'delete own no_page content',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in no_directorate'.
  $permissions['delete terms in no_directorate'] = array(
    'name' => 'delete terms in no_directorate',
    'roles' => array(
      'Developer' => 'Developer',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit file'.
  $permissions['edit file'] = array(
    'name' => 'edit file',
    'roles' => array(
      'Developer' => 'Developer',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'file_entity',
  );

  // Exported permission: 'edit own no_page content'.
  $permissions['edit own no_page content'] = array(
    'name' => 'edit own no_page content',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own no_promo content'.
  $permissions['edit own no_promo content'] = array(
    'name' => 'edit own no_promo content',
    'roles' => array(
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in no_directorate'.
  $permissions['edit terms in no_directorate'] = array(
    'name' => 'edit terms in no_directorate',
    'roles' => array(
      'Developer' => 'Developer',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'import media'.
  $permissions['import media'] = array(
    'name' => 'import media',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'media',
  );

  // Exported permission: 'publish own no_page content'.
  $permissions['publish own no_page content'] = array(
    'name' => 'publish own no_page content',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Section editor' => 'Section editor',
    ),
    'module' => 'publishcontent',
  );

  // Exported permission: 'search all content'.
  $permissions['search all content'] = array(
    'name' => 'search all content',
    'roles' => array(
      'Developer' => 'Developer',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search content'.
  $permissions['search content'] = array(
    'name' => 'search content',
    'roles' => array(
      'Developer' => 'Developer',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search',
  );

  // Exported permission: 'search no_article content'.
  $permissions['search no_article content'] = array(
    'name' => 'search no_article content',
    'roles' => array(
      'Developer' => 'Developer',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search no_event content'.
  $permissions['search no_event content'] = array(
    'name' => 'search no_event content',
    'roles' => array(
      'Developer' => 'Developer',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search no_milestone content'.
  $permissions['search no_milestone content'] = array(
    'name' => 'search no_milestone content',
    'roles' => array(
      'Developer' => 'Developer',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search no_office content'.
  $permissions['search no_office content'] = array(
    'name' => 'search no_office content',
    'roles' => array(
      'Developer' => 'Developer',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search no_page content'.
  $permissions['search no_page content'] = array(
    'name' => 'search no_page content',
    'roles' => array(
      'Developer' => 'Developer',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search no_partner content'.
  $permissions['search no_partner content'] = array(
    'name' => 'search no_partner content',
    'roles' => array(
      'Developer' => 'Developer',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search no_phonebook_entry content'.
  $permissions['search no_phonebook_entry content'] = array(
    'name' => 'search no_phonebook_entry content',
    'roles' => array(
      'Developer' => 'Developer',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search no_promo content'.
  $permissions['search no_promo content'] = array(
    'name' => 'search no_promo content',
    'roles' => array(
      'Developer' => 'Developer',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search no_section content'.
  $permissions['search no_section content'] = array(
    'name' => 'search no_section content',
    'roles' => array(
      'Developer' => 'Developer',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'skip menu restrictions'.
  $permissions['skip menu restrictions'] = array(
    'name' => 'skip menu restrictions',
    'roles' => array(
      'Developer' => 'Developer',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'no_admin',
  );

  // Exported permission: 'unpublish own no_page content'.
  $permissions['unpublish own no_page content'] = array(
    'name' => 'unpublish own no_page content',
    'roles' => array(),
    'module' => 'publishcontent',
  );

  // Exported permission: 'view any unpublished chess_file content'.
  $permissions['view any unpublished chess_file content'] = array(
    'name' => 'view any unpublished chess_file content',
    'roles' => array(
      'In-Kind moderator' => 'In-Kind moderator',
    ),
    'module' => 'view_unpublished',
  );

  // Exported permission: 'view any unpublished content'.
  $permissions['view any unpublished content'] = array(
    'name' => 'view any unpublished content',
    'roles' => array(),
    'module' => 'view_unpublished',
  );

  // Exported permission: 'view any unpublished no_page content'.
  $permissions['view any unpublished no_page content'] = array(
    'name' => 'view any unpublished no_page content',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Section editor' => 'Section editor',
    ),
    'module' => 'view_unpublished',
  );

  // Exported permission: 'view file'.
  $permissions['view file'] = array(
    'name' => 'view file',
    'roles' => array(
      'Developer' => 'Developer',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'file_entity',
  );

  // Exported permission: 'view own unpublished content'.
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Section editor' => 'Section editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view the administration theme'.
  $permissions['view the administration theme'] = array(
    'name' => 'view the administration theme',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'system',
  );

  return $permissions;
}
