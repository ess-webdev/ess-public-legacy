<?php
/**
 * @file
 * no_roles_and_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function no_roles_and_permissions_user_default_roles() {
  $roles = array();

  // Exported role: Content editor.
  $roles['Content editor'] = array(
    'name' => 'Content editor',
    'weight' => 2,
  );

  // Exported role: Section editor.
  $roles['Section editor'] = array(
    'name' => 'Section editor',
    'weight' => 3,
  );

  // Exported role: Site administrator.
  $roles['Site administrator'] = array(
    'name' => 'Site administrator',
    'weight' => 4,
  );

  return $roles;
}
