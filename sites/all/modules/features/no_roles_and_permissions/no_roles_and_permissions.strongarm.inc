<?php
/**
 * @file
 * no_roles_and_permissions.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function no_roles_and_permissions_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'publishcontent_no_page';
  $strongarm->value = 1;
  $export['publishcontent_no_page'] = $strongarm;

  return $export;
}
