<?php
/**
 * @file
 * Code for the Admin feature.
 */

include_once 'no_admin.features.inc';

/**
 * Implements hook_permission().
 */
function no_admin_permission() {
  return array(
    'skip menu restrictions' => array(
      'title' => t('Skip menu restrictions'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_user_insert().
 *
 * See function no_admin_og_grant_role().
 */
function no_admin_user_insert(&$edit, $account, $category) {
  no_admin_section_grant_role($account);
}

/**
 * Implements hook_user_update().
 *
 * See function no_admin_og_grant_role().
 */
function no_admin_user_update(&$edit, $account, $category) {
  no_admin_section_grant_role($account);
}

/**
 * When a user with the role section editor is created, we want to add that user
 * as an admin in all groups he or she is attached to.
 */
function no_admin_section_grant_role($account) {
  $user_groups = og_get_groups_by_user($account);
  $all_groups = og_get_groups_by_user();
  if (!empty($all_groups)) {
    foreach ($all_groups['node'] as $gid) {
      og_role_revoke('node', $gid, $account->uid, 6);
    }
  }
  og_invalidate_cache();
  if (in_array('4', $account->roles) && !empty($user_groups)) {
    foreach ($user_groups['node'] as $group_ref) {
      og_role_grant('node', $group_ref, $account->uid, 6);
    }
  }
}

/**
 * Implements hook_form_alter().
 * 
 * Add a javascript to the add user form to check checkboxes.
 */
function no_admin_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'user_register_form' || $form_id == 'user_profile_form') {
    $form['#attached']['js'] = array(
      drupal_get_path('module', 'no_admin') . '/no_admin.js',
    );
  }
  if (isset($form['#node_edit_form'])) {
    hide($form['options']['promote']);
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function no_admin_form_node_form_alter(&$form, &$form_state) {
  $form['#pre_render'][] = 'no_admin_form_node_alter_menu';
}

function no_admin_form_node_alter_menu(&$form) {
  if (!user_access('skip menu restrictions') && $form['type']['#value'] == 'no_page') {
    $og_groups = og_get_groups_by_user();
    if (isset($og_groups['node'])) {
      $link = $form['#node']->menu;
      $type = $form['#node']->type;
      $new_options = array();
      $menu_tree = menu_tree_all_data($link['menu_name']);
      _no_admin_get_options($link['menu_name'], $menu_tree, $og_groups['node'], $new_options);
      // Fetch data from our current menu tree.
      $form['menu']['link']['parent']['#options'] = $new_options;
    }
  }
  return $form;
}

/**
 * Our own function. Go through the menu tree and remove all links that are not
 * under the parent item for the og group that the logged in user belongs to.
 */
function _no_admin_get_options($menu_name, $tree, $og_groups, &$options, $prefix = '-- ', $allow = FALSE) {
  foreach ($tree as $link) {
    $item_allow = $allow;
    if (!$allow) {
      // @todo: consider performance issues!
      if (strstr($link['link']['link_path'], 'node') !== FALSE) {
        list(, $nid) = explode('/', $link['link']['link_path']);
        if (isset($og_groups[$nid])) {
          $options[$menu_name . ':' . $link['link']['mlid']] = $prefix . $link['link']['title'];
          $item_allow = TRUE;
        }
      }
    }
    else {
      $options[$menu_name . ':' . $link['link']['mlid']] = $prefix . $link['link']['title'];
    }
    if (!empty($link['below'])) {
      _no_admin_get_options($menu_name, $link['below'], $og_groups, $options, '-' . $prefix, $item_allow);
    }
  }
  return $options;
}
