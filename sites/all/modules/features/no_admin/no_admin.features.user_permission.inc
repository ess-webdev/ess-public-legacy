<?php
/**
 * @file
 * no_admin.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function no_admin_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer features'.
  $permissions['administer features'] = array(
    'name' => 'administer features',
    'roles' => array(
      'Developer' => 'Developer',
    ),
    'module' => 'features',
  );

  // Exported permission: 'generate features'.
  $permissions['generate features'] = array(
    'name' => 'generate features',
    'roles' => array(
      'Developer' => 'Developer',
    ),
    'module' => 'features',
  );

  // Exported permission: 'manage features'.
  $permissions['manage features'] = array(
    'name' => 'manage features',
    'roles' => array(
      'Developer' => 'Developer',
    ),
    'module' => 'features',
  );

  // Exported permission: 'rename features'.
  $permissions['rename features'] = array(
    'name' => 'rename features',
    'roles' => array(
      'Developer' => 'Developer',
    ),
    'module' => 'features',
  );

  return $permissions;
}
