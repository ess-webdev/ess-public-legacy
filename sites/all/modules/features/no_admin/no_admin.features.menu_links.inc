<?php
/**
 * @file
 * no_admin.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function no_admin_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-editor-administration-menu_add-content:node/add.
  $menu_links['menu-editor-administration-menu_add-content:node/add'] = array(
    'menu_name' => 'menu-editor-administration-menu',
    'link_path' => 'node/add',
    'router_path' => 'node/add',
    'link_title' => 'Add content',
    'options' => array(
      'attributes' => array(
        'title' => 'Create a new page.',
      ),
      'external' => 0,
      'identifier' => 'menu-editor-administration-menu_add-content:node/add',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: menu-editor-administration-menu_my-content:admin/user/content.
  $menu_links['menu-editor-administration-menu_my-content:admin/user/content'] = array(
    'menu_name' => 'menu-editor-administration-menu',
    'link_path' => 'admin/user/content',
    'router_path' => 'admin/user/content',
    'link_title' => 'My content',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'external' => 0,
      'identifier' => 'menu-editor-administration-menu_my-content:admin/user/content',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: menu-section-administration-menu_add-content:node/add.
  $menu_links['menu-section-administration-menu_add-content:node/add'] = array(
    'menu_name' => 'menu-section-administration-menu',
    'link_path' => 'node/add',
    'router_path' => 'node/add',
    'link_title' => 'Add content',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'external' => 0,
      'identifier' => 'menu-section-administration-menu_add-content:node/add',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: menu-section-administration-menu_my-content:admin/user/content.
  $menu_links['menu-section-administration-menu_my-content:admin/user/content'] = array(
    'menu_name' => 'menu-section-administration-menu',
    'link_path' => 'admin/user/content',
    'router_path' => 'admin/user/content',
    'link_title' => 'My content',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'external' => 0,
      'identifier' => 'menu-section-administration-menu_my-content:admin/user/content',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: menu-site-admin-menu_add-content:node/add.
  $menu_links['menu-site-admin-menu_add-content:node/add'] = array(
    'menu_name' => 'menu-site-admin-menu',
    'link_path' => 'node/add',
    'router_path' => 'node/add',
    'link_title' => 'Add content',
    'options' => array(
      'attributes' => array(
        'title' => 'Add content, i.e. pages or articles.',
      ),
      'external' => 0,
      'identifier' => 'menu-site-admin-menu_add-content:node/add',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 0,
  );
  // Exported menu link: menu-site-admin-menu_footer-menu:admin/structure/menu/manage/menu-footer.
  $menu_links['menu-site-admin-menu_footer-menu:admin/structure/menu/manage/menu-footer'] = array(
    'menu_name' => 'menu-site-admin-menu',
    'link_path' => 'admin/structure/menu/manage/menu-footer',
    'router_path' => 'admin/structure/menu/manage/%',
    'link_title' => 'Footer menu',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'external' => 0,
      'identifier' => 'menu-site-admin-menu_footer-menu:admin/structure/menu/manage/menu-footer',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 0,
  );
  // Exported menu link: menu-site-admin-menu_front-page:admin/content/front.
  $menu_links['menu-site-admin-menu_front-page:admin/content/front'] = array(
    'menu_name' => 'menu-site-admin-menu',
    'link_path' => 'admin/content/front',
    'router_path' => 'admin/content/front',
    'link_title' => 'Front page',
    'options' => array(
      'attributes' => array(
        'title' => 'Administer content on front page.',
      ),
      'external' => 0,
      'identifier' => 'menu-site-admin-menu_front-page:admin/content/front',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 0,
  );
  // Exported menu link: menu-site-admin-menu_main-menu:admin/structure/menu/manage/main-menu.
  $menu_links['menu-site-admin-menu_main-menu:admin/structure/menu/manage/main-menu'] = array(
    'menu_name' => 'menu-site-admin-menu',
    'link_path' => 'admin/structure/menu/manage/main-menu',
    'router_path' => 'admin/structure/menu/manage/%',
    'link_title' => 'Main menu',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'external' => 0,
      'identifier' => 'menu-site-admin-menu_main-menu:admin/structure/menu/manage/main-menu',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 0,
  );
  // Exported menu link: menu-site-admin-menu_manage-users:admin/people.
  $menu_links['menu-site-admin-menu_manage-users:admin/people'] = array(
    'menu_name' => 'menu-site-admin-menu',
    'link_path' => 'admin/people',
    'router_path' => 'admin/people',
    'link_title' => 'Manage users',
    'options' => array(
      'attributes' => array(
        'title' => 'Manage users',
      ),
      'external' => 0,
      'identifier' => 'menu-site-admin-menu_manage-users:admin/people',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 0,
  );
  // Exported menu link: menu-site-admin-menu_secondary-menu:admin/structure/menu/manage/menu-secondary-menu.
  $menu_links['menu-site-admin-menu_secondary-menu:admin/structure/menu/manage/menu-secondary-menu'] = array(
    'menu_name' => 'menu-site-admin-menu',
    'link_path' => 'admin/structure/menu/manage/menu-secondary-menu',
    'router_path' => 'admin/structure/menu/manage/%',
    'link_title' => 'Secondary menu',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'external' => 0,
      'identifier' => 'menu-site-admin-menu_secondary-menu:admin/structure/menu/manage/menu-secondary-menu',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 0,
  );
  // Exported menu link: menu-site-admin-menu_view-content:admin/content.
  $menu_links['menu-site-admin-menu_view-content:admin/content'] = array(
    'menu_name' => 'menu-site-admin-menu',
    'link_path' => 'admin/content',
    'router_path' => 'admin/content',
    'link_title' => 'View content',
    'options' => array(
      'attributes' => array(
        'title' => 'View the content that is currently on the site.',
      ),
      'external' => 0,
      'identifier' => 'menu-site-admin-menu_view-content:admin/content',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Add content');
  t('Footer menu');
  t('Front page');
  t('Main menu');
  t('Manage users');
  t('My content');
  t('Secondary menu');
  t('View content');

  return $menu_links;
}
