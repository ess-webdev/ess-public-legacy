<?php
/**
 * @file
 * no_admin.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function no_admin_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function no_admin_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function no_admin_flag_default_flags() {
  $flags = array();
  // Exported flag: "Promoted to front page".
  $flags['promote_front'] = array(
    'content_type' => 'node',
    'title' => 'Promoted to front page',
    'global' => 1,
    'types' => array(
      0 => 'no_article',
      1 => 'no_event',
      2 => 'no_promo',
    ),
    'flag_short' => 'Promote to front page',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Demote from front page',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'normal',
    'roles' => array(
      'flag' => array(
        0 => 5,
        1 => 9,
      ),
      'unflag' => array(
        0 => 5,
        1 => 9,
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 0,
    'show_on_form' => 1,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'no_admin',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}
