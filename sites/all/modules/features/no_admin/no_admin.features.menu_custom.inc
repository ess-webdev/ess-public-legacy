<?php
/**
 * @file
 * no_admin.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function no_admin_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-editor-administration-menu.
  $menus['menu-editor-administration-menu'] = array(
    'menu_name' => 'menu-editor-administration-menu',
    'title' => 'Editor administration menu',
    'description' => 'A menu for the content editor role.',
  );
  // Exported menu: menu-section-administration-menu.
  $menus['menu-section-administration-menu'] = array(
    'menu_name' => 'menu-section-administration-menu',
    'title' => 'Section administration menu',
    'description' => '',
  );
  // Exported menu: menu-site-admin-menu.
  $menus['menu-site-admin-menu'] = array(
    'menu_name' => 'menu-site-admin-menu',
    'title' => 'Site adminstration menu',
    'description' => 'A menu for the site administrator role.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('A menu for the content editor role.');
  t('A menu for the site administrator role.');
  t('Editor administration menu');
  t('Section administration menu');
  t('Site adminstration menu');

  return $menus;
}
