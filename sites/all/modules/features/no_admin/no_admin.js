(function($) {
  
  // Automatically check all the section checkboxes if you check the
  // "Site administrator" role checkbox.
  Drupal.behaviors.attachRoleToGroups = {
    attach : function(context, settings) {
      $('.form-item-roles-5 #edit-roles-5').click(function(){
        var groups = $('#edit-og-user-group-ref input');
        if ($(this).attr('checked') == true) {
          groups.attr('checked', 'checked');
        }
        else {
          groups.attr('checked', false);
        }
      });
    } 
  }

})(jQuery);
