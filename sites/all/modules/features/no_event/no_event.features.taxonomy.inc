<?php
/**
 * @file
 * no_event.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function no_event_taxonomy_default_vocabularies() {
    return FALSE;
}
