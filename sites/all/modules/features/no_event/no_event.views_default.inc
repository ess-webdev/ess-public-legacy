<?php
/**
 * @file
 * no_event.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function no_event_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'no_event';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Event';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Event details';
  $handler->display->display_options['css_class'] = 'details';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_no_date']['id'] = 'field_no_date';
  $handler->display->display_options['fields']['field_no_date']['table'] = 'field_data_field_no_date';
  $handler->display->display_options['fields']['field_no_date']['field'] = 'field_no_date';
  $handler->display->display_options['fields']['field_no_date']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['field_no_date']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_no_date']['element_type'] = 'span';
  $handler->display->display_options['fields']['field_no_date']['element_label_type'] = 'strong';
  $handler->display->display_options['fields']['field_no_date']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_no_date']['element_wrapper_class'] = 'event-item date';
  $handler->display->display_options['fields']['field_no_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Place */
  $handler->display->display_options['fields']['field_no_place']['id'] = 'field_no_place';
  $handler->display->display_options['fields']['field_no_place']['table'] = 'field_data_field_no_place';
  $handler->display->display_options['fields']['field_no_place']['field'] = 'field_no_place';
  $handler->display->display_options['fields']['field_no_place']['element_type'] = 'span';
  $handler->display->display_options['fields']['field_no_place']['element_label_type'] = 'strong';
  $handler->display->display_options['fields']['field_no_place']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_no_place']['element_wrapper_class'] = 'event-item place';
  $handler->display->display_options['fields']['field_no_place']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_place']['hide_empty'] = TRUE;
  /* Field: Content: Link */
  $handler->display->display_options['fields']['field_no_link']['id'] = 'field_no_link';
  $handler->display->display_options['fields']['field_no_link']['table'] = 'field_data_field_no_link';
  $handler->display->display_options['fields']['field_no_link']['field'] = 'field_no_link';
  $handler->display->display_options['fields']['field_no_link']['element_type'] = 'span';
  $handler->display->display_options['fields']['field_no_link']['element_label_type'] = 'strong';
  $handler->display->display_options['fields']['field_no_link']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_no_link']['element_wrapper_class'] = 'event-item link';
  $handler->display->display_options['fields']['field_no_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_link']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_no_link']['click_sort_column'] = 'url';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<img src="http://maps.googleapis.com/maps/api/staticmap?center=[field_no_place]&zoom=15&size=290x200&scale=1&maptype=roadmap%20&markers=color:blue|label:S|40.702147,-74.015794&markers=color:green|label:G|40.711614,-74.012318%20&markers=color:red|color:red|label:C|40.718217,-73.998284&sensor=false" />';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'http://maps.google.com/maps?daddr=[field_no_place]&source=embed&z=15';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['nothing']['element_wrapper_class'] = 'event-item map';
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['nothing']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['nothing']['hide_alter_empty'] = TRUE;
  /* Field: Content: Place */
  $handler->display->display_options['fields']['field_no_place_1']['id'] = 'field_no_place_1';
  $handler->display->display_options['fields']['field_no_place_1']['table'] = 'field_data_field_no_place';
  $handler->display->display_options['fields']['field_no_place_1']['field'] = 'field_no_place';
  $handler->display->display_options['fields']['field_no_place_1']['label'] = '';
  $handler->display->display_options['fields']['field_no_place_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_no_place_1']['alter']['text'] = '<img src="http://maps.googleapis.com/maps/api/staticmap?center=[field_no_place]&zoom=15&size=290x200&scale=1&maptype=roadmap%20&markers=color:blue|label:S|40.702147,-74.015794&markers=color:green|label:G|40.711614,-74.012318%20&markers=color:red|color:red|label:C|40.718217,-73.998284&sensor=false" />';
  $handler->display->display_options['fields']['field_no_place_1']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_no_place_1']['alter']['path'] = 'http://maps.google.com/maps?daddr=[field_no_place]&source=embed&z=15';
  $handler->display->display_options['fields']['field_no_place_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_place_1']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_no_place_1']['element_wrapper_class'] = 'event-item map';
  $handler->display->display_options['fields']['field_no_place_1']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_place_1']['hide_empty'] = TRUE;
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'no_event' => 'no_event',
  );

  /* Display: Event info */
  $handler = $view->new_display('panel_pane', 'Event info', 'panel_pane_1');
  $handler->display->display_options['argument_input'] = array(
    'nid' => array(
      'type' => 'context',
      'context' => 'entity:node.nid',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Content: Nid',
    ),
  );
  $export['no_event'] = $view;

  $view = new view();
  $view->name = 'no_events';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Events';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Events';
  $handler->display->display_options['css_class'] = 'events';
  $handler->display->display_options['use_more_always'] = TRUE;
  $handler->display->display_options['use_more_text'] = 'All events';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Relationship: OG membership: OG membership from Node */
  $handler->display->display_options['relationships']['og_membership_rel']['id'] = 'og_membership_rel';
  $handler->display->display_options['relationships']['og_membership_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['og_membership_rel']['field'] = 'og_membership_rel';
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['path']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['path']['absolute'] = TRUE;
  /* Field: Content: Media */
  $handler->display->display_options['fields']['field_no_media']['id'] = 'field_no_media';
  $handler->display->display_options['fields']['field_no_media']['table'] = 'field_data_field_no_media';
  $handler->display->display_options['fields']['field_no_media']['field'] = 'field_no_media';
  $handler->display->display_options['fields']['field_no_media']['label'] = '';
  $handler->display->display_options['fields']['field_no_media']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_no_media']['alter']['path'] = '[path]';
  $handler->display->display_options['fields']['field_no_media']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_media']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_media']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_no_media']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_no_media']['settings'] = array(
    'file_view_mode' => 'media_small',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_no_date']['id'] = 'field_no_date';
  $handler->display->display_options['fields']['field_no_date']['table'] = 'field_data_field_no_date';
  $handler->display->display_options['fields']['field_no_date']['field'] = 'field_no_date';
  $handler->display->display_options['fields']['field_no_date']['label'] = '';
  $handler->display->display_options['fields']['field_no_date']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['field_no_date']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_no_date']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_no_date']['element_class'] = 'meta';
  $handler->display->display_options['fields']['field_no_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'value',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Lead */
  $handler->display->display_options['fields']['field_no_lead']['id'] = 'field_no_lead';
  $handler->display->display_options['fields']['field_no_lead']['table'] = 'field_data_field_no_lead';
  $handler->display->display_options['fields']['field_no_lead']['field'] = 'field_no_lead';
  $handler->display->display_options['fields']['field_no_lead']['label'] = '';
  $handler->display->display_options['fields']['field_no_lead']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['field_no_lead']['alter']['max_length'] = '200';
  $handler->display->display_options['fields']['field_no_lead']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_no_lead']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_no_lead']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_no_lead']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_lead']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_lead']['type'] = 'text_plain';
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['element_type'] = 'p';
  $handler->display->display_options['fields']['view_node']['element_class'] = 'more';
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['view_node']['text'] = 'Read more';
  /* Sort criterion: Content: Date -  start date (field_no_date) */
  $handler->display->display_options['sorts']['field_no_date_value']['id'] = 'field_no_date_value';
  $handler->display->display_options['sorts']['field_no_date_value']['table'] = 'field_data_field_no_date';
  $handler->display->display_options['sorts']['field_no_date_value']['field'] = 'field_no_date_value';
  /* Contextual filter: OG membership: Group ID */
  $handler->display->display_options['arguments']['gid']['id'] = 'gid';
  $handler->display->display_options['arguments']['gid']['table'] = 'og_membership';
  $handler->display->display_options['arguments']['gid']['field'] = 'gid';
  $handler->display->display_options['arguments']['gid']['relationship'] = 'og_membership_rel';
  $handler->display->display_options['arguments']['gid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['gid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['gid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['gid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['gid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'no_event' => 'no_event',
  );
  /* Filter criterion: Content: Date -  start date (field_no_date) */
  $handler->display->display_options['filters']['field_no_date_value']['id'] = 'field_no_date_value';
  $handler->display->display_options['filters']['field_no_date_value']['table'] = 'field_data_field_no_date';
  $handler->display->display_options['filters']['field_no_date_value']['field'] = 'field_no_date_value';
  $handler->display->display_options['filters']['field_no_date_value']['operator'] = '!=';
  $handler->display->display_options['filters']['field_no_date_value']['default_date'] = 'now - 1 day';

  /* Display: Events */
  $handler = $view->new_display('panel_pane', 'Events', 'panel_pane_1');
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'gid' => array(
      'type' => 'context',
      'context' => 'entity:node.nid',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'OG membership: Group ID',
    ),
  );

  /* Display: Events */
  $handler = $view->new_display('panel_pane', 'Events', 'panel_pane_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Events';
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'items';
  $handler->display->display_options['defaults']['use_ajax'] = FALSE;
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['defaults']['use_more'] = FALSE;
  $handler->display->display_options['use_more'] = TRUE;
  $handler->display->display_options['defaults']['link_display'] = FALSE;
  $handler->display->display_options['link_display'] = 'custom_url';
  $handler->display->display_options['link_url'] = 'node/!1/events';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h5';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_no_date']['id'] = 'field_no_date';
  $handler->display->display_options['fields']['field_no_date']['table'] = 'field_data_field_no_date';
  $handler->display->display_options['fields']['field_no_date']['field'] = 'field_no_date';
  $handler->display->display_options['fields']['field_no_date']['label'] = '';
  $handler->display->display_options['fields']['field_no_date']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_no_date']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_no_date']['element_class'] = 'meta';
  $handler->display->display_options['fields']['field_no_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'no_event' => 'no_event',
  );
  /* Filter criterion: Content: Date - end date (field_no_date:value2) */
  $handler->display->display_options['filters']['field_no_date_value2']['id'] = 'field_no_date_value2';
  $handler->display->display_options['filters']['field_no_date_value2']['table'] = 'field_data_field_no_date';
  $handler->display->display_options['filters']['field_no_date_value2']['field'] = 'field_no_date_value2';
  $handler->display->display_options['filters']['field_no_date_value2']['operator'] = '>=';
  $handler->display->display_options['filters']['field_no_date_value2']['default_date'] = 'now';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'gid' => array(
      'type' => 'context',
      'context' => 'entity:node.nid',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'OG membership: Group ID',
    ),
  );
  $export['no_events'] = $view;

  return $export;
}
