<?php
/**
 * @file
 * no_content_rest_server.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function no_content_rest_server_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_views_api().
 */
function no_content_rest_server_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
