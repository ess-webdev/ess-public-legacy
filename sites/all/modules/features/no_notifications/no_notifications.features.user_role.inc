<?php
/**
 * @file
 * no_notifications.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function no_notifications_user_default_roles() {
  $roles = array();

  // Exported role: ILO.
  $roles['ILO'] = array(
    'name' => 'ILO',
    'weight' => 10,
  );

  return $roles;
}
