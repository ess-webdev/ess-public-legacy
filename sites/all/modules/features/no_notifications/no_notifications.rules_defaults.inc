<?php
/**
 * @file
 * no_notifications.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function no_notifications_default_rules_configuration() {
  $items = array();
  $items['rules_notify_ilos_of_update_to_procurement_page'] = entity_import('rules_config', '{ "rules_notify_ilos_of_update_to_procurement_page" : {
      "LABEL" : "Notify ILOs of update to Procurement page",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "node_update--no_page" : { "bundle" : "no_page" } },
      "IF" : [
        { "data_is" : { "data" : [ "node:nid" ], "value" : "2150" } },
        { "flag_flagged_node" : { "flag" : "notification", "node" : [ "node" ], "user" : "1" } }
      ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "11" : "11" } },
            "subject" : "ESS Procurement Update",
            "message" : "Dear ILO,\\r\\n\\r\\nThe ESS Procurement page has been updated with a new call for tender: [node:url]#updated\\r\\n\\r\\nRegards,\\r\\nESS ILO Network\\r\\n\\r\\nThis is an auto-generated message.",
            "from" : "ilo@esss.se"
          }
        },
        { "flag_unflagnode" : {
            "flag" : "notification",
            "node" : [ "node" ],
            "flagging_user" : "1",
            "permission_check" : "1"
          }
        }
      ]
    }
  }');
  return $items;
}
