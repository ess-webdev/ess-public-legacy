<?php
/**
 * @file
 * no_notifications.features.inc
 */

/**
 * Implements hook_flag_default_flags().
 */
function no_notifications_flag_default_flags() {
  $flags = array();
  // Exported flag: "Notification".
  $flags['notification'] = array(
    'content_type' => 'node',
    'title' => 'Notification',
    'global' => 1,
    'types' => array(
      0 => 'no_page',
    ),
    'flag_short' => 'Send notifications for this update',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Do not send notifications for this update',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => 3,
        1 => 5,
      ),
      'unflag' => array(
        0 => 3,
        1 => 5,
      ),
    ),
    'show_on_page' => 0,
    'show_on_teaser' => 0,
    'show_on_form' => 1,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'no_notifications',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}
