<?php
/**
 * @file
 * no_deployment_plan_default_sections.deploy_plans.inc
 */

/**
 * Implements hook_deploy_plans_default().
 */
function no_deployment_plan_default_sections_deploy_plans_default() {
  $export = array();

  $plan = new DeployPlan();
  $plan->disabled = FALSE; /* Edit this to true to make a default plan disabled initially */
  $plan->api_version = 1;
  $plan->name = 'section_nodes';
  $plan->title = 'Section nodes';
  $plan->description = 'A plan for providing default Section nodes right after the installation.';
  $plan->debug = 0;
  $plan->aggregator_plugin = 'DeployAggregatorManaged';
  $plan->aggregator_config = array(
    'view_name' => 'section_content',
  );
  $plan->fetch_only = 1;
  $plan->processor_plugin = '';
  $plan->processor_config = array();
  $plan->endpoints = array();
  $export['section_nodes'] = $plan;

  return $export;
}
