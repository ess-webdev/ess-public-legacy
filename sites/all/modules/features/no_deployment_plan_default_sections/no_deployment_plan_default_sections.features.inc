<?php
/**
 * @file
 * no_deployment_plan_default_sections.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function no_deployment_plan_default_sections_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "deploy" && $api == "deploy_plans") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function no_deployment_plan_default_sections_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
