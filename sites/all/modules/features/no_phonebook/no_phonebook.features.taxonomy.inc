<?php
/**
 * @file
 * no_phonebook.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function no_phonebook_taxonomy_default_vocabularies() {
  return array(
    'no_directorate' => array(
      'name' => 'Directorate',
      'machine_name' => 'no_directorate',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'no_division' => array(
      'name' => 'Division',
      'machine_name' => 'no_division',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
