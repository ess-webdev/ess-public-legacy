<?php

/**
 * @file no_phonebook_feeds_plugin_ess_querypath_parser.inc
 */

class ESSFeedsQueryPathParser extends FeedsQueryPathParser {

  /**
   * Implements FeedsQueryPathParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    // Setup mappings so they can be used in variable replacement.
    $mappings = $source->importer->processor->config['mappings'];
    $mappings = $this->filterMappings($mappings);

    // Set source config, if it's empty get config from importer.
    $this->source_config = $source->getConfigFor($this);

    // Allow config inheritance.
    if (empty($this->source_config)) {
      $this->source_config = $this->config;
    }
    $this->rawXML = array_keys(array_filter($this->source_config['raw']['rawXML']));
    $this->debug = array_keys(array_filter($this->source_config['debug']['options']));


    $raw = trim($fetcher_result->getRaw());

    // Removes weird characters. Basically, this is the only extra thing what we do, all
    // the other functionality is the same as in the parent class.
    $raw = mb_substr($raw, 2, mb_strlen($raw));
    $raw = str_replace(chr(13), '', $raw);
    $raw = str_replace(chr(0), '', $raw);

    if (empty($raw)) {
      throw new Exception(t('Feeds QueryPath parser: The document is empty.'));
    }

    $result = new FeedsParserResult();
    // Set link so we can set the result link attribute.
    $fetcher_config = $source->getConfigFor($source->importer->fetcher);
    $result->link = $fetcher_config['source'];

    $opts = array(
      'ignore_parser_warnings' => TRUE,
    );
    $doc = @qp($raw, NULL, $opts);

    $result->title = qp($doc, 'title', $opts)->text();
    $context = qp($doc, $this->source_config['context'], $opts);
    $this->debug($context, 'context');

    foreach ($context as $item) {
      $parsed_item = $variables = array();
      foreach ($this->source_config['sources'] as $source => $query) {
        $parsed = $this->parseSourceElement($item, $query, $source);
        // Avoid null values.
        if (isset($parsed)) {
          // Variable sunstitution can't handle arrays.
          if (!is_array($parsed)) {
            $variables['{' . $mappings[$source] . '}'] = $parsed;
          }
          else {
            $variables['{' . $mappings[$source] . '}'] = '';
          }
          $parsed_item[$source] = $parsed;
        }
      }
      $result->items[] = $parsed_item;
    }
    return $result;
  }

}

