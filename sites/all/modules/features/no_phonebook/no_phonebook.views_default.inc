<?php
/**
 * @file
 * no_phonebook.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function no_phonebook_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'no_phone_book';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Phone Book';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '40';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_no_pb_job_title' => 'field_no_pb_job_title',
    'field_no_pb_email' => 'field_no_pb_email',
    'field_no_pb_phone' => 'field_no_pb_phone',
    'field_no_pb_mobile' => 'field_no_pb_mobile',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_no_pb_job_title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_no_pb_email' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_no_pb_phone' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_no_pb_mobile' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'Please enter the name of the ESS employee you are looking for above.';
  $handler->display->display_options['empty']['area']['format'] = 'no_text_editor';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Name';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Job title */
  $handler->display->display_options['fields']['field_no_pb_job_title']['id'] = 'field_no_pb_job_title';
  $handler->display->display_options['fields']['field_no_pb_job_title']['table'] = 'field_data_field_no_pb_job_title';
  $handler->display->display_options['fields']['field_no_pb_job_title']['field'] = 'field_no_pb_job_title';
  $handler->display->display_options['fields']['field_no_pb_job_title']['element_label_colon'] = FALSE;
  /* Field: Content: Email */
  $handler->display->display_options['fields']['field_no_pb_email']['id'] = 'field_no_pb_email';
  $handler->display->display_options['fields']['field_no_pb_email']['table'] = 'field_data_field_no_pb_email';
  $handler->display->display_options['fields']['field_no_pb_email']['field'] = 'field_no_pb_email';
  $handler->display->display_options['fields']['field_no_pb_email']['element_label_colon'] = FALSE;
  /* Field: Content: Phone number */
  $handler->display->display_options['fields']['field_no_pb_phone']['id'] = 'field_no_pb_phone';
  $handler->display->display_options['fields']['field_no_pb_phone']['table'] = 'field_data_field_no_pb_phone';
  $handler->display->display_options['fields']['field_no_pb_phone']['field'] = 'field_no_pb_phone';
  $handler->display->display_options['fields']['field_no_pb_phone']['element_label_colon'] = FALSE;
  /* Field: Content: Mobile phone number */
  $handler->display->display_options['fields']['field_no_pb_mobile']['id'] = 'field_no_pb_mobile';
  $handler->display->display_options['fields']['field_no_pb_mobile']['table'] = 'field_data_field_no_pb_mobile';
  $handler->display->display_options['fields']['field_no_pb_mobile']['field'] = 'field_no_pb_mobile';
  $handler->display->display_options['fields']['field_no_pb_mobile']['label'] = 'Mobile';
  $handler->display->display_options['fields']['field_no_pb_mobile']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Sort criterion: Content: Last name (field_no_pb_last_name) */
  $handler->display->display_options['sorts']['field_no_pb_last_name_value']['id'] = 'field_no_pb_last_name_value';
  $handler->display->display_options['sorts']['field_no_pb_last_name_value']['table'] = 'field_data_field_no_pb_last_name';
  $handler->display->display_options['sorts']['field_no_pb_last_name_value']['field'] = 'field_no_pb_last_name_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'no_phonebook_entry' => 'no_phonebook_entry',
  );
  /* Filter criterion: Content: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'contains';
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Name';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['required'] = TRUE;

  /* Display: Phone book */
  $handler = $view->new_display('panel_pane', 'Phone book', 'panel_pane_1');
  $handler->display->display_options['inherit_panels_path'] = '1';
  $export['no_phone_book'] = $view;

  return $export;
}
