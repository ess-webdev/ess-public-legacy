<?php
/**
 * @file
 * no_phonebook.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function no_phonebook_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = TRUE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'ess_phonebook';
  $feeds_importer->config = array(
    'name' => 'ESS Phonebook',
    'description' => 'Imports phonebook entries from an XML source.',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'ESSFeedsQueryPathParser',
      'config' => array(
        'context' => 'ms',
        'sources' => array(
          'querypathparser:0' => 'i32[n="uidNumber"]',
          'querypathparser:1' => 's[n="Name"]',
          'querypathparser:2' => 's[n="FirstName"]',
          'querypathparser:3' => 's[n="LastName"]',
          'querypathparser:4' => 's[n="Title"]',
          'querypathparser:5' => 's[n="Department"]',
          'querypathparser:6' => 's[n="mail"]',
          'querypathparser:7' => 's[n="mobile"]',
          'querypathparser:8' => 's[n="PostalCode"]',
          'querypathparser:9' => 's[n="street"]',
          'querypathparser:10' => 's[n="l"]',
          'querypathparser:11' => 's[n="division"]',
          'querypathparser:12' => 's[n="Company"]',
          'querypathparser:13' => 's[n="telephoneNumber"]',
        ),
        'debug' => array(
          'options' => array(
            'context' => 0,
            'querypathparser:0' => 0,
            'querypathparser:1' => 0,
            'querypathparser:2' => 0,
            'querypathparser:3' => 0,
            'querypathparser:4' => 0,
            'querypathparser:5' => 0,
            'querypathparser:6' => 0,
            'querypathparser:7' => 0,
            'querypathparser:8' => 0,
            'querypathparser:9' => 0,
            'querypathparser:10' => 0,
            'querypathparser:11' => 0,
            'querypathparser:12' => 0,
            'querypathparser:13' => 0,
          ),
        ),
        'attrs' => array(
          'querypathparser:0' => '',
          'querypathparser:1' => '',
          'querypathparser:2' => '',
          'querypathparser:3' => '',
          'querypathparser:4' => '',
          'querypathparser:5' => '',
          'querypathparser:6' => '',
          'querypathparser:7' => '',
          'querypathparser:8' => '',
          'querypathparser:9' => '',
          'querypathparser:10' => '',
          'querypathparser:11' => '',
          'querypathparser:12' => '',
          'querypathparser:13' => '',
        ),
        'rawXML' => array(
          'querypathparser:0' => 0,
          'querypathparser:1' => 0,
          'querypathparser:2' => 0,
          'querypathparser:3' => 0,
          'querypathparser:4' => 0,
          'querypathparser:5' => 0,
          'querypathparser:6' => 0,
          'querypathparser:7' => 0,
          'querypathparser:8' => 0,
          'querypathparser:9' => 0,
          'querypathparser:10' => 0,
          'querypathparser:11' => 0,
          'querypathparser:12' => 0,
          'querypathparser:13' => 0,
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'no_phonebook_entry',
        'expire' => '-1',
        'author' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'querypathparser:0',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'querypathparser:1',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'querypathparser:2',
            'target' => 'field_no_pb_first_name',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'querypathparser:3',
            'target' => 'field_no_pb_last_name',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'querypathparser:4',
            'target' => 'field_no_pb_job_title',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'querypathparser:5',
            'target' => 'field_no_pb_division',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'querypathparser:6',
            'target' => 'field_no_pb_email',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'querypathparser:7',
            'target' => 'field_no_pb_mobile',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'querypathparser:8',
            'target' => 'field_no_pb_postal_code',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'querypathparser:9',
            'target' => 'field_no_pb_street_address',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'querypathparser:10',
            'target' => 'field_no_pb_location',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'querypathparser:11',
            'target' => 'field_no_pb_directorate',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'querypathparser:12',
            'target' => 'field_no_pb_company',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'querypathparser:13',
            'target' => 'field_no_pb_phone',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'update_non_existant' => '5',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['ess_phonebook'] = $feeds_importer;

  return $export;
}
