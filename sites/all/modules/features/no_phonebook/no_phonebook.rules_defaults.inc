<?php
/**
 * @file
 * no_phonebook.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function no_phonebook_default_rules_configuration() {
  $items = array();
  $items['rules_alter_phonebook_entries'] = entity_import('rules_config', '{ "rules_alter_phonebook_entries" : {
      "LABEL" : "Alter imported phonebook entries",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "feeds" ],
      "ON" : { "feeds_import_ad_import" : [] },
      "IF" : [
        { "text_matches" : { "text" : [ "node:field-no-pb-last-name" ], "match" : "Womersley" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-no-pb-mobile" ] } },
        { "data_set" : { "data" : [ "node:field-no-pb-phone" ] } },
        { "data_set" : { "data" : [ "node:field-no-pb-email" ] } }
      ]
    }
  }');
  return $items;
}
