<?php
/**
 * @file
 * no_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function no_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_accelerator:node/5.
  $menu_links['main-menu_accelerator:node/5'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/5',
    'router_path' => 'node/%',
    'link_title' => 'Accelerator',
    'options' => array(
      'external' => 0,
      'attributes' => array(
        'title' => 'Accelerator',
      ),
      'identifier' => 'main-menu_accelerator:node/5',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_technology:node/3389',
  );
  // Exported menu link: main-menu_building-ess:node/6.
  $menu_links['main-menu_building-ess:node/6'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/6',
    'router_path' => 'node/%',
    'link_title' => 'Building ESS',
    'options' => array(
      'attributes' => array(
        'title' => 'Building ESS',
      ),
      'external' => 0,
      'identifier' => 'main-menu_building-ess:node/6',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 0,
  );
  // Exported menu link: main-menu_careers:node/7.
  $menu_links['main-menu_careers:node/7'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/7',
    'router_path' => 'node/%',
    'link_title' => 'Careers',
    'options' => array(
      'external' => 0,
      'identifier' => 'main-menu_careers:node/7',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 0,
  );
  // Exported menu link: main-menu_science--instruments:node/3.
  $menu_links['main-menu_science--instruments:node/3'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/3',
    'router_path' => 'node/%',
    'link_title' => 'Science & Instruments',
    'options' => array(
      'attributes' => array(),
      'external' => 0,
      'identifier' => 'main-menu_science--instruments:node/3',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 0,
  );
  // Exported menu link: main-menu_target:node/4.
  $menu_links['main-menu_target:node/4'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/4',
    'router_path' => 'node/%',
    'link_title' => 'Target',
    'options' => array(
      'external' => 0,
      'attributes' => array(),
      'identifier' => 'main-menu_target:node/4',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_technology:node/3389',
  );
  // Exported menu link: main-menu_vacancies:node/9.
  $menu_links['main-menu_vacancies:node/9'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/9',
    'router_path' => 'node/%',
    'link_title' => 'Vacancies',
    'options' => array(
      'external' => 0,
      'identifier' => 'main-menu_vacancies:node/9',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -40,
    'customized' => 0,
    'parent_identifier' => 'main-menu_careers:node/7',
  );
  // Exported menu link: menu-secondary-menu_phone-book:phone-book.
  $menu_links['menu-secondary-menu_phone-book:phone-book'] = array(
    'menu_name' => 'menu-secondary-menu',
    'link_path' => 'phone-book',
    'router_path' => 'phone-book',
    'link_title' => 'Phone book',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'external' => 0,
      'identifier' => 'menu-secondary-menu_phone-book:phone-book',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 0,
  );
  // Exported menu link: menu-social_connect-with-us-on-linkedin:http://www.linkedin.com/company/326252.
  $menu_links['menu-social_connect-with-us-on-linkedin:http://www.linkedin.com/company/326252'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://www.linkedin.com/company/326252',
    'router_path' => '',
    'link_title' => 'Connect with us on LinkedIn',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'external' => 1,
      'identifier' => 'menu-social_connect-with-us-on-linkedin:http://www.linkedin.com/company/326252',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 0,
  );
  // Exported menu link: menu-social_join-us-on-facebook:http://www.facebook.com/europeanspallationsource.
  $menu_links['menu-social_join-us-on-facebook:http://www.facebook.com/europeanspallationsource'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://www.facebook.com/europeanspallationsource',
    'router_path' => '',
    'link_title' => 'Join us on Facebook',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'external' => 1,
      'identifier' => 'menu-social_join-us-on-facebook:http://www.facebook.com/europeanspallationsource',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Accelerator');
  t('Building ESS');
  t('Careers');
  t('Connect with us on LinkedIn');
  t('Join us on Facebook');
  t('Phone book');
  t('Science & Instruments');
  t('Target');
  t('Vacancies');

  return $menu_links;
}
