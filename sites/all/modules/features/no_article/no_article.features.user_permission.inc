<?php
/**
 * @file
 * no_article.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function no_article_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer manualcrop settings'.
  $permissions['administer manualcrop settings'] = array(
    'name' => 'administer manualcrop settings',
    'roles' => array(
      'Developer' => 'Developer',
    ),
    'module' => 'manualcrop',
  );

  // Exported permission: 'use manualcrop'.
  $permissions['use manualcrop'] = array(
    'name' => 'use manualcrop',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Developer' => 'Developer',
      'FDadm' => 'FDadm',
      'FDemt' => 'FDemt',
      'FDepg' => 'FDepg',
      'Section editor' => 'Section editor',
      'Site administrator' => 'Site administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'manualcrop',
  );

  return $permissions;
}
