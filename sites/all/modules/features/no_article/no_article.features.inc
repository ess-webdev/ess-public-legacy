<?php
/**
 * @file
 * no_article.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function no_article_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function no_article_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function no_article_image_default_styles() {
  $styles = array();

  // Exported image style: featured_article.
  $styles['featured_article'] = array(
    'label' => 'featured_article',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 350,
          'height' => 287,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: full-width_crop.
  $styles['full-width_crop'] = array(
    'label' => 'Full-width crop',
    'effects' => array(
      1 => array(
        'name' => 'manualcrop_crop',
        'data' => array(
          'width' => '',
          'height' => '',
          'keepproportions' => 0,
          'reuse_crop_style' => '',
          'style_name' => 'full-width_crop',
        ),
        'weight' => 0,
      ),
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 620,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: related.
  $styles['related'] = array(
    'label' => 'Related (198x144)',
    'effects' => array(
      6 => array(
        'name' => 'manualcrop_reuse',
        'data' => array(
          'reuse_crop_style' => 'full-width_crop',
          'apply_all_effects' => 0,
        ),
        'weight' => 0,
      ),
      7 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 198,
          'height' => 144,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: section_overview.
  $styles['section_overview'] = array(
    'label' => 'Section overview other (220 x 160)',
    'effects' => array(
      1 => array(
        'name' => 'manualcrop_reuse',
        'data' => array(
          'reuse_crop_style' => 'full-width_crop',
          'apply_all_effects' => 0,
        ),
        'weight' => 0,
      ),
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 220,
          'height' => 160,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: section_overview_featured__300_x_220_.
  $styles['section_overview_featured__300_x_220_'] = array(
    'label' => 'Section overview featured (300 x 220)',
    'effects' => array(
      3 => array(
        'name' => 'manualcrop_reuse',
        'data' => array(
          'reuse_crop_style' => 'full-width_crop',
          'apply_all_effects' => 0,
        ),
        'weight' => 0,
      ),
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 300,
          'height' => 220,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function no_article_node_info() {
  $items = array(
    'no_article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
