<?php
/**
 * @file
 * no_article.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function no_article_taxonomy_default_vocabularies() {
    return FALSE;
}
