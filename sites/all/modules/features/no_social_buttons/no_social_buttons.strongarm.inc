<?php
/**
 * @file
 * no_social_buttons.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function no_social_buttons_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addthis_bookmark_url';
  $strongarm->value = 'https://www.addthis.com/bookmark.php?v=300';
  $export['addthis_bookmark_url'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addthis_custom_configuration_code';
  $strongarm->value = 'var addthis_config = {}';
  $export['addthis_custom_configuration_code'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addthis_custom_configuration_code_enabled';
  $strongarm->value = 0;
  $export['addthis_custom_configuration_code_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addthis_services_css_url';
  $strongarm->value = 'https://cache.addthiscdn.com/icons/v1/sprites/services.css';
  $export['addthis_services_css_url'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addthis_services_json_url';
  $strongarm->value = 'https://cache.addthiscdn.com/services/v1/sharing.en.json';
  $export['addthis_services_json_url'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addthis_widget_include';
  $strongarm->value = '2';
  $export['addthis_widget_include'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addthis_widget_js_url';
  $strongarm->value = 'https://s7.addthis.com/js/300/addthis_widget.js';
  $export['addthis_widget_js_url'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addthis_widget_load_async';
  $strongarm->value = 0;
  $export['addthis_widget_load_async'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'addthis_widget_load_domready';
  $strongarm->value = 1;
  $export['addthis_widget_load_domready'] = $strongarm;

  return $export;
}
