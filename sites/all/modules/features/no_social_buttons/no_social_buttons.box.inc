<?php
/**
 * @file
 * no_social_buttons.box.inc
 */

/**
 * Implements hook_default_box().
 */
function no_social_buttons_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'social_sidebar';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Social sidebar';
  $box->options = array(
    'body' => array(
      'value' => '<div class="addthis_toolbox">
  <div class="social_sidebar_label">SHARE</div>
  <div class="custom_images">
    <a class="addthis_button_facebook"></a>
    <a class="addthis_button_twitter"></a>
    <a class="addthis_button_linkedin"></a>
  </div>
</div>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['social_sidebar'] = $box;

  return $export;
}
