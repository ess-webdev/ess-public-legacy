<?php
/**
 * @file
 * no_text.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function no_text_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'use text format filtered_html'.
  $permissions['use text format filtered_html'] = array(
    'name' => 'use text format filtered_html',
    'roles' => array(
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'filter',
  );

  // Exported permission: 'use text format no_text_editor'.
  $permissions['use text format no_text_editor'] = array(
    'name' => 'use text format no_text_editor',
    'roles' => array(
      'Content editor' => 'Content editor',
      'Developer' => 'Developer',
      'Section editor' => 'Section editor',
      'Site administrator' => 'Site administrator',
    ),
    'module' => 'filter',
  );

  return $permissions;
}
