<?php
/**
 * @file
 * no_contact.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function no_contact_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_no_office';
  $strongarm->value = array();
  $export['menu_options_no_office'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_no_office';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_no_office'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_no_office';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_no_office'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_no_office';
  $strongarm->value = '1';
  $export['node_preview_no_office'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_no_office';
  $strongarm->value = 0;
  $export['node_submitted_no_office'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_no_office_pattern';
  $strongarm->value = 'office/[node:title]';
  $export['pathauto_node_no_office_pattern'] = $strongarm;

  return $export;
}
