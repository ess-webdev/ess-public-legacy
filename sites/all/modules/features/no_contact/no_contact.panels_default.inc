<?php
/**
 * @file
 * no_contact.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function no_contact_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'no_contact';
  $mini->category = '';
  $mini->admin_title = 'Contact';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '159',
      'id' => 1,
    ),
    1 => array(
      'identifier' => 'Token',
      'keyword' => 'token',
      'name' => 'token',
      'id' => 1,
    ),
  );
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'naked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'main' => NULL,
      'aside' => NULL,
      'left' => array(
        'content_tag' => 'div',
        'content_id' => '',
        'content_class' => 'contact-info',
      ),
      'right' => array(
        'content_tag' => 'div',
        'content_id' => '',
        'content_class' => 'contact-image',
      ),
    ),
    'left' => array(
      'style' => 'element',
    ),
    'right' => array(
      'style' => 'element',
    ),
  );
  $display->cache = array();
  $display->title = 'Get in touch';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1';
    $pane->panel = 'main';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<h5>%token:site:name</h5><p>%node:field_no_address</p><p>Telephone: %node:field_no_phone</p><p>Visiting address: %node:field_no_visiting_address</p>',
      'format' => 'full_html',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'style' => 'naked',
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $display->content['new-1'] = $pane;
    $display->panels['main'][0] = 'new-1';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['no_contact'] = $mini;

  return $export;
}
