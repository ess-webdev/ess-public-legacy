<?php
/**
 * @file
 * f_downloads.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function f_downloads_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'additional_settings__active_tab_download';
  $strongarm->value = 'edit-og';
  $export['additional_settings__active_tab_download'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__download';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '1',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__download'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'filedepot_locked_file_download_enabled';
  $strongarm->value = 1;
  $export['filedepot_locked_file_download_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_download';
  $strongarm->value = array();
  $export['menu_options_download'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_download_area';
  $strongarm->value = array(
    'main-menu' => 'main-menu',
    'devel' => 0,
    'menu-editor-administration-menu' => 0,
    'menu-footer' => 0,
    'management' => 0,
    'navigation' => 0,
    'menu-quick-navigation' => 0,
    'menu-secondary-menu' => 0,
    'menu-section-administration-menu' => 0,
    'menu-site-admin-menu' => 0,
    'menu-social' => 0,
    'user-menu' => 0,
  );
  $export['menu_options_download_area'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_download_category';
  $strongarm->value = array(
    'main-menu' => 'main-menu',
    'menu-editor-administration-menu' => 0,
    'menu-footer' => 0,
    'management' => 0,
    'navigation' => 0,
    'menu-secondary-menu' => 0,
    'menu-section-administration-menu' => 0,
    'menu-site-admin-menu' => 0,
    'menu-social' => 0,
    'user-menu' => 0,
  );
  $export['menu_options_download_category'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_download';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_download'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_download_area';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_download_area'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_download_category';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_download_category'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_download';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_download'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_download';
  $strongarm->value = '0';
  $export['node_preview_download'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_download';
  $strongarm->value = 0;
  $export['node_submitted_download'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_download_pattern';
  $strongarm->value = 'download/[node:title]';
  $export['pathauto_node_download_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'save_continue_download';
  $strongarm->value = 'Save and add fields';
  $export['save_continue_download'] = $strongarm;

  return $export;
}
