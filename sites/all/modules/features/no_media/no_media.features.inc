<?php
/**
 * @file
 * no_media.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function no_media_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function no_media_image_default_styles() {
  $styles = array();

  // Exported image style: default.
  $styles['default'] = array(
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 620,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'default',
  );

  // Exported image style: feature.
  $styles['feature'] = array(
    'effects' => array(
      12 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 300,
          'height' => 220,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'feature',
  );

  // Exported image style: logo.
  $styles['logo'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 40,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'logo',
  );

  // Exported image style: milestone.
  $styles['milestone'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 470,
          'height' => 420,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'milestone',
  );

  // Exported image style: panorama.
  $styles['panorama'] = array(
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1000,
          'height' => 265,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'panorama',
  );

  // Exported image style: preview.
  $styles['preview'] = array(
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 140,
          'height' => 100,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'preview',
  );

  // Exported image style: promo.
  $styles['promo'] = array(
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 220,
          'height' => 130,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'promo',
  );

  // Exported image style: small.
  $styles['small'] = array(
    'effects' => array(
      5 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 220,
          'height' => 160,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'small',
  );

  // Exported image style: video_feature.
  $styles['video_feature'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 320,
        ),
        'weight' => -9,
      ),
      2 => array(
        'name' => 'image_crop',
        'data' => array(
          'width' => 300,
          'height' => 220,
          'anchor' => 'center-center',
        ),
        'weight' => -8,
      ),
    ),
    'label' => 'video_feature',
  );

  // Exported image style: video_small.
  $styles['video_small'] = array(
    'effects' => array(
      17 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 300,
          'height' => 240,
        ),
        'weight' => -10,
      ),
      16 => array(
        'name' => 'image_crop',
        'data' => array(
          'width' => 220,
          'height' => 160,
          'anchor' => 'center-center',
        ),
        'weight' => -9,
      ),
    ),
    'label' => 'video_small',
  );

  // Exported image style: wide.
  $styles['wide'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 460,
          'height' => 259,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'wide',
  );

  return $styles;
}
