<?php
/**
 * @file
 * no_timeline.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function no_timeline_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function no_timeline_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function no_timeline_flag_default_flags() {
  $flags = array();
  // Exported flag: "Featured".
  $flags['feature'] = array(
    'content_type' => 'node',
    'title' => 'Featured',
    'global' => 1,
    'types' => array(
      0 => 'no_milestone',
    ),
    'flag_short' => 'Featured',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Not featured',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => 2,
      ),
      'unflag' => array(
        0 => 2,
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_on_form' => 1,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'no_timeline',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Status".
  $flags['status'] = array(
    'content_type' => 'node',
    'title' => 'Status',
    'global' => 1,
    'types' => array(
      0 => 'no_milestone',
    ),
    'flag_short' => 'Completed',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Not completed',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => 2,
      ),
      'unflag' => array(
        0 => 2,
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 0,
    'show_on_form' => 1,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'no_timeline',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}

/**
 * Implements hook_node_info().
 */
function no_timeline_node_info() {
  $items = array(
    'no_milestone' => array(
      'name' => t('Milestone'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
