<?php
/**
 * @file
 * no_timeline.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function no_timeline_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_no_milestone';
  $strongarm->value = array();
  $export['menu_options_no_milestone'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_no_milestone';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_no_milestone'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_no_milestone';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_no_milestone'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_no_milestone';
  $strongarm->value = '1';
  $export['node_preview_no_milestone'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_no_milestone';
  $strongarm->value = 0;
  $export['node_submitted_no_milestone'] = $strongarm;

  return $export;
}
