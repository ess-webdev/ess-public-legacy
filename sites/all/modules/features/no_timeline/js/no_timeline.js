(function($) {

  Drupal.behaviors.timeline = {
    attach : function (context, settings) {

      $(".timeline ul").draggable({
        axis: 'x',
        snap: true,
        cursor: 'move',
        scrollSensitivity: 10,
        scrollSpeed: 10,
        drag: function(event, ui) {
          $('.timeline ul').addClass('dragging');
        },
        stop: Drupal.timelineSetActiveYear,
      });

      $(window).resize(Drupal.timelineSetContainment);

      $('.timeline ul').isotope({
        layoutMode : 'fitColumns',
        rowHeight: 420,
        itemClass: '',
        transformsEnabled: false
      });

      var timeline = $(".timeline ul");
      var interval = 50;

      var scroll = function() {
        var left = parseInt(timeline.css('left')) - 1;

        if ((timeline.parent().width() - timeline.width()) < left) {
          timeline.css('left', left);

          Drupal.timelineSetActiveYear();
        }
      }

      var intId = setInterval(scroll, interval);

      timeline.hover(function() {
        clearInterval(intId);
      }, function() {
        intId = setInterval(scroll, interval);
      });

      $('.timeline-nav li').click(function(e) {
        e.preventDefault();

        var clickedLink = $('a', $(this));

        // Set the 'active' class to the clicked link.
        $('.timeline-nav li a').removeClass('active');
        clickedLink.addClass('active');

        // Move the timeline to the first occurence of the clicked year.
        var year = clickedLink.html();
        var milestones = $('.timeline ul li.' + year);
        var position = parseInt($(milestones[0]).css('left'));

        var left = $('.timeline ul').width() - $('.timeline').width();

        if (position > left) {
          position = left;
        }

        clearInterval(intId);

        $('.timeline ul').animate({left: -position}, 500, 'swing', function() {
          intId = setInterval(scroll, interval);
        });
      });

      // Needs to be run after isotop.
      Drupal.timelineSetContainment();

      // Scroll baby, scroll!
      //Drupal.timelineAutoScroll(50);
    }
  }

  Drupal.timelineAutoScroll = function(interval) {
    var timeline = $(".timeline ul");

    var scroll = function() {
      var left = parseInt(timeline.css('left')) - 1;

      if ((timeline.parent().width() - timeline.width()) < left) {
        timeline.css('left', left);

        Drupal.timelineSetActiveYear();
      }
    }

    var intId = setInterval(scroll, interval);

    timeline.hover(function() {
      clearInterval(intId);
    }, function() {
      intId = setInterval(scroll, interval);
    });
  }

  Drupal.timelineSetContainment = function() {
    var timeline = $('.timeline ul');

    var c_w = timeline.width();
    var p_w = timeline.parent().width()

    if ((c_w + parseInt(timeline.css('left'))) < p_w) {
      timeline.css('left', (p_w - c_w));
    }

    timeline.draggable('option', 'containment', [-(c_w - p_w), 0, 0, 0]);
  }

  Drupal.timelineSetActiveYear = function() {
    var draggedPosition = parseInt($('.timeline ul').css('left')) * -1 + 200;

    var listItems = $('.timeline ul li');
    var previousListItem = listItems[0];
    var previousListItemPosition = 0;
    var currentListItemPosition;

    listItems.each(function(index, currentListItem) {
      currentListItemPosition = parseInt($(currentListItem).css('left'));
      if (currentListItemPosition >= draggedPosition) {
        return false;
      }
      if (currentListItemPosition > previousListItemPosition) {
        previousListItem = currentListItem;
        previousListItemPosition = currentListItemPosition;
      }
    });

    var classes = $(previousListItem).attr('class');
    var year = classes.match(/\d{4}/);

    $('.timeline-nav li a').removeClass('active');
    $('.timeline-nav li.' + year + ' a').addClass('active');
  }


  Drupal.behaviors.timelineNavJustified = {
    attach : function (context, settings) {
      var item_width = 'width: ' + 100 / $('.timeline-nav li').length + '%';

      $('.timeline-nav li').each(function() {
        $(this).attr('style', item_width);
      })
    }
  }

})(jQuery);

