<?php
/**
 * @file
 * no_timeline.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function no_timeline_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'no_timeline';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Timeline';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['css_class'] = 'timeline';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class'] = '[count]';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Relationship: Flags: status */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'status';
  $handler->display->display_options['relationships']['flag_content_rel']['user_scope'] = 'any';
  /* Relationship: Flags: feature */
  $handler->display->display_options['relationships']['flag_content_rel_1']['id'] = 'flag_content_rel_1';
  $handler->display->display_options['relationships']['flag_content_rel_1']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel_1']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel_1']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel_1']['flag'] = 'feature';
  $handler->display->display_options['relationships']['flag_content_rel_1']['user_scope'] = 'any';
  /* Relationship: Flags: status counter */
  $handler->display->display_options['relationships']['flag_count_rel']['id'] = 'flag_count_rel';
  $handler->display->display_options['relationships']['flag_count_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_count_rel']['field'] = 'flag_count_rel';
  $handler->display->display_options['relationships']['flag_count_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_count_rel']['flag'] = 'status';
  /* Relationship: Flags: feature counter */
  $handler->display->display_options['relationships']['flag_count_rel_1']['id'] = 'flag_count_rel_1';
  $handler->display->display_options['relationships']['flag_count_rel_1']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_count_rel_1']['field'] = 'flag_count_rel';
  $handler->display->display_options['relationships']['flag_count_rel_1']['required'] = 0;
  $handler->display->display_options['relationships']['flag_count_rel_1']['flag'] = 'feature';
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_no_milestone_date']['id'] = 'field_no_milestone_date';
  $handler->display->display_options['fields']['field_no_milestone_date']['table'] = 'field_data_field_no_milestone_date';
  $handler->display->display_options['fields']['field_no_milestone_date']['field'] = 'field_no_milestone_date';
  $handler->display->display_options['fields']['field_no_milestone_date']['label'] = '';
  $handler->display->display_options['fields']['field_no_milestone_date']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_no_milestone_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_milestone_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_milestone_date']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<span class="normal">[field_no_milestone_date]</span>
<span class="rollover"><span class="date">[field_no_milestone_date]</span><span class="title">[title]</span></span>';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '#';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Flags: Flag counter */
  $handler->display->display_options['fields']['count']['id'] = 'count';
  $handler->display->display_options['fields']['count']['table'] = 'flag_counts';
  $handler->display->display_options['fields']['count']['field'] = 'count';
  $handler->display->display_options['fields']['count']['relationship'] = 'flag_count_rel';
  $handler->display->display_options['fields']['count']['label'] = '';
  $handler->display->display_options['fields']['count']['exclude'] = TRUE;
  $handler->display->display_options['fields']['count']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['count']['alter']['text'] = 'completed';
  $handler->display->display_options['fields']['count']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['count']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['count']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['count']['empty_zero'] = TRUE;
  /* Sort criterion: Content: Date (field_no_milestone_date) */
  $handler->display->display_options['sorts']['field_no_milestone_date_value']['id'] = 'field_no_milestone_date_value';
  $handler->display->display_options['sorts']['field_no_milestone_date_value']['table'] = 'field_data_field_no_milestone_date';
  $handler->display->display_options['sorts']['field_no_milestone_date_value']['field'] = 'field_no_milestone_date_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'no_milestone' => 'no_milestone',
  );

  /* Display: Timeline */
  $handler = $view->new_display('panel_pane', 'Timeline', 'panel_pane_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['enabled'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class'] = '[field_no_milestone_date_1] [count_1]';
  $handler->display->display_options['style_options']['class'] = 'clearfix';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Flags: Flag counter */
  $handler->display->display_options['fields']['count']['id'] = 'count';
  $handler->display->display_options['fields']['count']['table'] = 'flag_counts';
  $handler->display->display_options['fields']['count']['field'] = 'count';
  $handler->display->display_options['fields']['count']['relationship'] = 'flag_count_rel';
  $handler->display->display_options['fields']['count']['label'] = '';
  $handler->display->display_options['fields']['count']['exclude'] = TRUE;
  $handler->display->display_options['fields']['count']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['count']['alter']['text'] = 'completed';
  $handler->display->display_options['fields']['count']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['count']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['count']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['count']['empty_zero'] = TRUE;
  /* Field: Flags: Flag counter */
  $handler->display->display_options['fields']['count_1']['id'] = 'count_1';
  $handler->display->display_options['fields']['count_1']['table'] = 'flag_counts';
  $handler->display->display_options['fields']['count_1']['field'] = 'count';
  $handler->display->display_options['fields']['count_1']['relationship'] = 'flag_count_rel_1';
  $handler->display->display_options['fields']['count_1']['label'] = '';
  $handler->display->display_options['fields']['count_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['count_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['count_1']['alter']['text'] = 'featured';
  $handler->display->display_options['fields']['count_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['count_1']['element_default_classes'] = FALSE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_no_milestone_date_1']['id'] = 'field_no_milestone_date_1';
  $handler->display->display_options['fields']['field_no_milestone_date_1']['table'] = 'field_data_field_no_milestone_date';
  $handler->display->display_options['fields']['field_no_milestone_date_1']['field'] = 'field_no_milestone_date';
  $handler->display->display_options['fields']['field_no_milestone_date_1']['label'] = '';
  $handler->display->display_options['fields']['field_no_milestone_date_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_no_milestone_date_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_no_milestone_date_1']['alter']['text'] = '[field_no_milestone_date_1-value]';
  $handler->display->display_options['fields']['field_no_milestone_date_1']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['field_no_milestone_date_1']['alter']['max_length'] = '4';
  $handler->display->display_options['fields']['field_no_milestone_date_1']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['field_no_milestone_date_1']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['field_no_milestone_date_1']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_no_milestone_date_1']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_no_milestone_date_1']['alter']['html'] = TRUE;
  $handler->display->display_options['fields']['field_no_milestone_date_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_milestone_date_1']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_milestone_date_1']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nid']['element_default_classes'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_no_image']['id'] = 'field_no_image';
  $handler->display->display_options['fields']['field_no_image']['table'] = 'field_data_field_no_image';
  $handler->display->display_options['fields']['field_no_image']['field'] = 'field_no_image';
  $handler->display->display_options['fields']['field_no_image']['label'] = '';
  $handler->display->display_options['fields']['field_no_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_no_image']['settings'] = array(
    'image_style' => 'milestone',
    'image_link' => '',
  );
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_no_milestone_date']['id'] = 'field_no_milestone_date';
  $handler->display->display_options['fields']['field_no_milestone_date']['table'] = 'field_data_field_no_milestone_date';
  $handler->display->display_options['fields']['field_no_milestone_date']['field'] = 'field_no_milestone_date';
  $handler->display->display_options['fields']['field_no_milestone_date']['label'] = '';
  $handler->display->display_options['fields']['field_no_milestone_date']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_no_milestone_date']['alter']['text'] = 'sticky';
  $handler->display->display_options['fields']['field_no_milestone_date']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['field_no_milestone_date']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['field_no_milestone_date']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['field_no_milestone_date']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_no_milestone_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_milestone_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_milestone_date']['settings'] = array(
    'format_type' => 'medium',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<span class="icon"></span>
<p class="date">[field_no_milestone_date]</p>
<h2>[title]</h2>
<p>[body]</p>
<p class="status">[count]</p>';
  $handler->display->display_options['fields']['title']['alter']['path'] = '#';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_wrapper_type'] = 'span';
  $handler->display->display_options['fields']['title']['element_wrapper_class'] = 'text';
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Sticky */
  $handler->display->display_options['fields']['sticky']['id'] = 'sticky';
  $handler->display->display_options['fields']['sticky']['table'] = 'node';
  $handler->display->display_options['fields']['sticky']['field'] = 'sticky';
  $handler->display->display_options['fields']['sticky']['label'] = '';
  $handler->display->display_options['fields']['sticky']['exclude'] = TRUE;
  $handler->display->display_options['fields']['sticky']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['sticky']['alter']['text'] = 'featured';
  $handler->display->display_options['fields']['sticky']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['sticky']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['sticky']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['sticky']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['sticky']['type'] = 'sticky';
  $handler->display->display_options['fields']['sticky']['not'] = 0;

  /* Display: Navigation */
  $handler = $view->new_display('panel_pane', 'Navigation', 'panel_pane_2');
  $handler->display->display_options['enabled'] = FALSE;
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'timeline-nav';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['class'] = 'clearfix';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_no_milestone_date']['id'] = 'field_no_milestone_date';
  $handler->display->display_options['fields']['field_no_milestone_date']['table'] = 'field_data_field_no_milestone_date';
  $handler->display->display_options['fields']['field_no_milestone_date']['field'] = 'field_no_milestone_date';
  $handler->display->display_options['fields']['field_no_milestone_date']['label'] = '';
  $handler->display->display_options['fields']['field_no_milestone_date']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_no_milestone_date']['alter']['path'] = '#[field_no_milestone_date]';
  $handler->display->display_options['fields']['field_no_milestone_date']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['field_no_milestone_date']['alter']['max_length'] = '4';
  $handler->display->display_options['fields']['field_no_milestone_date']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_no_milestone_date']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_no_milestone_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_milestone_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_milestone_date']['settings'] = array(
    'format_type' => 'year',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Date (field_no_milestone_date) */
  $handler->display->display_options['arguments']['field_no_milestone_date_value']['id'] = 'field_no_milestone_date_value';
  $handler->display->display_options['arguments']['field_no_milestone_date_value']['table'] = 'field_data_field_no_milestone_date';
  $handler->display->display_options['arguments']['field_no_milestone_date_value']['field'] = 'field_no_milestone_date_value';
  $handler->display->display_options['arguments']['field_no_milestone_date_value']['default_action'] = 'summary';
  $handler->display->display_options['arguments']['field_no_milestone_date_value']['default_argument_type'] = 'date';
  $handler->display->display_options['arguments']['field_no_milestone_date_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_no_milestone_date_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_no_milestone_date_value']['summary_options']['count'] = FALSE;
  $handler->display->display_options['arguments']['field_no_milestone_date_value']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_no_milestone_date_value']['granularity'] = 'year';
  $handler->display->display_options['arguments']['field_no_milestone_date_value']['title_format'] = 'custom';
  $handler->display->display_options['arguments']['field_no_milestone_date_value']['title_format_custom'] = 'Y';
  $export['no_timeline'] = $view;

  return $export;
}
