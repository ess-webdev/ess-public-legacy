<?php
/**
 * @file
 * no_front.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function no_front_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'no_front_articles';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Front articles';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'News';
  $handler->display->display_options['css_class'] = 'articles';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Relationship: Flags: promote_front */
  $handler->display->display_options['relationships']['flag_content_rel_1']['id'] = 'flag_content_rel_1';
  $handler->display->display_options['relationships']['flag_content_rel_1']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel_1']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel_1']['flag'] = 'promote_front';
  $handler->display->display_options['relationships']['flag_content_rel_1']['user_scope'] = 'any';
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['path']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['path']['absolute'] = TRUE;
  /* Field: Content: Media */
  $handler->display->display_options['fields']['field_no_media']['id'] = 'field_no_media';
  $handler->display->display_options['fields']['field_no_media']['table'] = 'field_data_field_no_media';
  $handler->display->display_options['fields']['field_no_media']['field'] = 'field_no_media';
  $handler->display->display_options['fields']['field_no_media']['label'] = '';
  $handler->display->display_options['fields']['field_no_media']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_no_media']['alter']['path'] = '[path]';
  $handler->display->display_options['fields']['field_no_media']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_media']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_media']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_no_media']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_no_media']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_no_media']['settings'] = array(
    'file_view_mode' => 'media_small',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = '';
  $handler->display->display_options['fields']['created']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['created']['alter']['text'] = '[created] <em>[field_no_section]</em>';
  $handler->display->display_options['fields']['created']['element_type'] = 'span';
  $handler->display->display_options['fields']['created']['element_class'] = 'meta';
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['created']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created']['custom_date_format'] = 'M j, Y';
  /* Field: Content: Lead */
  $handler->display->display_options['fields']['field_no_lead']['id'] = 'field_no_lead';
  $handler->display->display_options['fields']['field_no_lead']['table'] = 'field_data_field_no_lead';
  $handler->display->display_options['fields']['field_no_lead']['field'] = 'field_no_lead';
  $handler->display->display_options['fields']['field_no_lead']['label'] = '';
  $handler->display->display_options['fields']['field_no_lead']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['field_no_lead']['alter']['max_length'] = '200';
  $handler->display->display_options['fields']['field_no_lead']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_no_lead']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_no_lead']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_no_lead']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_lead']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_lead']['type'] = 'text_plain';
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['element_type'] = 'p';
  $handler->display->display_options['fields']['view_node']['element_class'] = 'more';
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['view_node']['text'] = 'Read more';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'no_article' => 'no_article',
  );
  /* Filter criterion: Content: Sticky */
  $handler->display->display_options['filters']['sticky']['id'] = 'sticky';
  $handler->display->display_options['filters']['sticky']['table'] = 'node';
  $handler->display->display_options['filters']['sticky']['field'] = 'sticky';
  $handler->display->display_options['filters']['sticky']['value'] = '0';

  /* Display: Articles */
  $handler = $view->new_display('panel_pane', 'Articles', 'panel_pane_1');

  /* Display: Featured article */
  $handler = $view->new_display('attachment', 'Featured article', 'attachment_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'featured-article';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['path']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['path']['absolute'] = TRUE;
  /* Field: Content: Media */
  $handler->display->display_options['fields']['field_no_media']['id'] = 'field_no_media';
  $handler->display->display_options['fields']['field_no_media']['table'] = 'field_data_field_no_media';
  $handler->display->display_options['fields']['field_no_media']['field'] = 'field_no_media';
  $handler->display->display_options['fields']['field_no_media']['label'] = '';
  $handler->display->display_options['fields']['field_no_media']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_no_media']['alter']['path'] = '[path]';
  $handler->display->display_options['fields']['field_no_media']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_media']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_no_media']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_no_media']['settings'] = array(
    'file_view_mode' => 'token',
  );
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_image']['element_class'] = 'featured-image';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'frontpage_crop',
    'image_link' => 'content',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = '';
  $handler->display->display_options['fields']['created']['element_type'] = 'span';
  $handler->display->display_options['fields']['created']['element_class'] = 'meta';
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['created']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created']['custom_date_format'] = 'M j, Y';
  /* Field: Content: Kicker */
  $handler->display->display_options['fields']['field_no_kicker']['id'] = 'field_no_kicker';
  $handler->display->display_options['fields']['field_no_kicker']['table'] = 'field_data_field_no_kicker';
  $handler->display->display_options['fields']['field_no_kicker']['field'] = 'field_no_kicker';
  $handler->display->display_options['fields']['field_no_kicker']['label'] = '';
  $handler->display->display_options['fields']['field_no_kicker']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_no_kicker']['alter']['text'] = '<p class="kicker-field">[field_no_kicker]</p>';
  $handler->display->display_options['fields']['field_no_kicker']['element_label_colon'] = FALSE;
  /* Field: Content: Lead */
  $handler->display->display_options['fields']['field_no_lead']['id'] = 'field_no_lead';
  $handler->display->display_options['fields']['field_no_lead']['table'] = 'field_data_field_no_lead';
  $handler->display->display_options['fields']['field_no_lead']['field'] = 'field_no_lead';
  $handler->display->display_options['fields']['field_no_lead']['label'] = '';
  $handler->display->display_options['fields']['field_no_lead']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_no_lead']['alter']['text'] = '<p class="kicker-field">[field_no_kicker]</p><p class="big lead">[field_no_lead]</p>';
  $handler->display->display_options['fields']['field_no_lead']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['field_no_lead']['alter']['max_length'] = '370';
  $handler->display->display_options['fields']['field_no_lead']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_no_lead']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_no_lead']['element_class'] = 'lead';
  $handler->display->display_options['fields']['field_no_lead']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_lead']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_lead']['type'] = 'text_plain';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['text'] = '<p>[body]</p>';
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['max_length'] = '200';
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['body']['element_type'] = 'p';
  $handler->display->display_options['fields']['body']['element_class'] = 'body';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['element_type'] = 'p';
  $handler->display->display_options['fields']['view_node']['element_class'] = 'more';
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['view_node']['text'] = 'Read more';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'no_article' => 'no_article',
  );
  /* Filter criterion: Content: Sticky */
  $handler->display->display_options['filters']['sticky']['id'] = 'sticky';
  $handler->display->display_options['filters']['sticky']['table'] = 'node';
  $handler->display->display_options['filters']['sticky']['field'] = 'sticky';
  $handler->display->display_options['filters']['sticky']['value'] = '1';
  $handler->display->display_options['displays'] = array(
    'panel_pane_1' => 'panel_pane_1',
    'default' => 0,
    'panel_pane_2' => 0,
  );
  $export['no_front_articles'] = $view;

  $view = new view();
  $view->name = 'no_front_events';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Front events';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Events';
  $handler->display->display_options['css_class'] = 'items';
  $handler->display->display_options['use_more_always'] = TRUE;
  $handler->display->display_options['use_more_text'] = 'All events';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Relationship: Flags: promote_front */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'promote_front';
  $handler->display->display_options['relationships']['flag_content_rel']['user_scope'] = 'any';
  /* Field: Content: Categories */
  $handler->display->display_options['fields']['field_categories']['id'] = 'field_categories';
  $handler->display->display_options['fields']['field_categories']['table'] = 'field_data_field_categories';
  $handler->display->display_options['fields']['field_categories']['field'] = 'field_categories';
  $handler->display->display_options['fields']['field_categories']['label'] = '';
  $handler->display->display_options['fields']['field_categories']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_categories']['alter']['text'] = '<h4>[field_categories]</h4>';
  $handler->display->display_options['fields']['field_categories']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_categories']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_categories']['delta_offset'] = '0';
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['path']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['path']['absolute'] = TRUE;
  /* Field: Content: Media */
  $handler->display->display_options['fields']['field_no_media']['id'] = 'field_no_media';
  $handler->display->display_options['fields']['field_no_media']['table'] = 'field_data_field_no_media';
  $handler->display->display_options['fields']['field_no_media']['field'] = 'field_no_media';
  $handler->display->display_options['fields']['field_no_media']['label'] = '';
  $handler->display->display_options['fields']['field_no_media']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_no_media']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_no_media']['alter']['path'] = '[path]';
  $handler->display->display_options['fields']['field_no_media']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_media']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_media']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_no_media']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_no_media']['settings'] = array(
    'file_view_mode' => 'media_preview',
  );
  /* Field: Content: Place */
  $handler->display->display_options['fields']['field_no_place']['id'] = 'field_no_place';
  $handler->display->display_options['fields']['field_no_place']['table'] = 'field_data_field_no_place';
  $handler->display->display_options['fields']['field_no_place']['field'] = 'field_no_place';
  $handler->display->display_options['fields']['field_no_place']['label'] = '';
  $handler->display->display_options['fields']['field_no_place']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_no_place']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_place']['element_default_classes'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h5';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_no_date']['id'] = 'field_no_date';
  $handler->display->display_options['fields']['field_no_date']['table'] = 'field_data_field_no_date';
  $handler->display->display_options['fields']['field_no_date']['field'] = 'field_no_date';
  $handler->display->display_options['fields']['field_no_date']['label'] = '';
  $handler->display->display_options['fields']['field_no_date']['alter']['text'] = '<em>[field_no_place]</em> [field_no_date]';
  $handler->display->display_options['fields']['field_no_date']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['field_no_date']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_no_date']['alter']['preserve_tags'] = '<a>';
  $handler->display->display_options['fields']['field_no_date']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_no_date']['element_class'] = 'meta';
  $handler->display->display_options['fields']['field_no_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Lead */
  $handler->display->display_options['fields']['field_no_lead']['id'] = 'field_no_lead';
  $handler->display->display_options['fields']['field_no_lead']['table'] = 'field_data_field_no_lead';
  $handler->display->display_options['fields']['field_no_lead']['field'] = 'field_no_lead';
  $handler->display->display_options['fields']['field_no_lead']['label'] = '';
  $handler->display->display_options['fields']['field_no_lead']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_no_lead']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['field_no_lead']['alter']['max_length'] = '200';
  $handler->display->display_options['fields']['field_no_lead']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_no_lead']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_no_lead']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_no_lead']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_no_lead']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_no_lead']['type'] = 'text_plain';
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['exclude'] = TRUE;
  $handler->display->display_options['fields']['view_node']['element_type'] = 'p';
  $handler->display->display_options['fields']['view_node']['element_class'] = 'more';
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['view_node']['text'] = 'Read more';
  /* Sort criterion: Content: Date -  start date (field_no_date) */
  $handler->display->display_options['sorts']['field_no_date_value']['id'] = 'field_no_date_value';
  $handler->display->display_options['sorts']['field_no_date_value']['table'] = 'field_data_field_no_date';
  $handler->display->display_options['sorts']['field_no_date_value']['field'] = 'field_no_date_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'no_event' => 'no_event',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Events */
  $handler = $view->new_display('panel_pane', 'Events', 'panel_pane_1');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'no_event' => 'no_event',
  );
  /* Filter criterion: Content: Date - end date (field_no_date:value2) */
  $handler->display->display_options['filters']['field_no_date_value2']['id'] = 'field_no_date_value2';
  $handler->display->display_options['filters']['field_no_date_value2']['table'] = 'field_data_field_no_date';
  $handler->display->display_options['filters']['field_no_date_value2']['field'] = 'field_no_date_value2';
  $handler->display->display_options['filters']['field_no_date_value2']['operator'] = '>=';
  $handler->display->display_options['filters']['field_no_date_value2']['default_date'] = 'now';
  $handler->display->display_options['argument_input'] = array(
    'field_no_section_tid' => array(
      'type' => 'context',
      'context' => 'entity:taxonomy_term.tid',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Content: Section (field_no_section)',
    ),
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['content'] = 'There are no ESS events scheduled at this time.';
  $handler->display->display_options['empty']['area']['format'] = 'no_text_editor';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'no_event' => 'no_event',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Date - end date (field_no_date:value2) */
  $handler->display->display_options['filters']['field_no_date_value2']['id'] = 'field_no_date_value2';
  $handler->display->display_options['filters']['field_no_date_value2']['table'] = 'field_data_field_no_date';
  $handler->display->display_options['filters']['field_no_date_value2']['field'] = 'field_no_date_value2';
  $handler->display->display_options['filters']['field_no_date_value2']['operator'] = '>=';
  $handler->display->display_options['filters']['field_no_date_value2']['expose']['operator_id'] = 'field_no_date_value2_op';
  $handler->display->display_options['filters']['field_no_date_value2']['expose']['label'] = 'Date - end date (field_no_date:value2)';
  $handler->display->display_options['filters']['field_no_date_value2']['expose']['operator'] = 'field_no_date_value2_op';
  $handler->display->display_options['filters']['field_no_date_value2']['expose']['identifier'] = 'field_no_date_value2';
  $handler->display->display_options['filters']['field_no_date_value2']['default_date'] = 'now';
  /* Filter criterion: Flags: Flagged */
  $handler->display->display_options['filters']['flagged']['id'] = 'flagged';
  $handler->display->display_options['filters']['flagged']['table'] = 'flag_content';
  $handler->display->display_options['filters']['flagged']['field'] = 'flagged';
  $handler->display->display_options['filters']['flagged']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['filters']['flagged']['value'] = '1';
  $export['no_front_events'] = $view;

  return $export;
}
