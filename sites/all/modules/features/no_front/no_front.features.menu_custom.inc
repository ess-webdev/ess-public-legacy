<?php
/**
 * @file
 * no_front.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function no_front_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-quick-navigation.
  $menus['menu-quick-navigation'] = array(
    'menu_name' => 'menu-quick-navigation',
    'title' => 'Quick navigation',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Quick navigation');

  return $menus;
}
