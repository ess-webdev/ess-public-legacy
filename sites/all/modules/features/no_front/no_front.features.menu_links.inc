<?php
/**
 * @file
 * no_front.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function no_front_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-quick-navigation_events:<void4>.
  $menu_links['menu-quick-navigation_events:<void4>'] = array(
    'menu_name' => 'menu-quick-navigation',
    'link_path' => '<void4>',
    'router_path' => '<void4>',
    'link_title' => 'Events',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'menu-quick-navigation_events:<void4>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 0,
  );
  // Exported menu link: menu-quick-navigation_news:<void1>.
  $menu_links['menu-quick-navigation_news:<void1>'] = array(
    'menu_name' => 'menu-quick-navigation',
    'link_path' => '<void1>',
    'router_path' => '<void1>',
    'link_title' => 'News',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'menu-quick-navigation_news:<void1>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 0,
  );
  // Exported menu link: menu-quick-navigation_partners:<void2>.
  $menu_links['menu-quick-navigation_partners:<void2>'] = array(
    'menu_name' => 'menu-quick-navigation',
    'link_path' => '<void2>',
    'router_path' => '<void2>',
    'link_title' => 'Partners',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'menu-quick-navigation_partners:<void2>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 0,
  );
  // Exported menu link: menu-quick-navigation_procurement:<void5>.
  $menu_links['menu-quick-navigation_procurement:<void5>'] = array(
    'menu_name' => 'menu-quick-navigation',
    'link_path' => '<void5>',
    'router_path' => '<void5>',
    'link_title' => 'Procurement',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'menu-quick-navigation_procurement:<void5>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 0,
  );
  // Exported menu link: menu-quick-navigation_science:<void3>.
  $menu_links['menu-quick-navigation_science:<void3>'] = array(
    'menu_name' => 'menu-quick-navigation',
    'link_path' => '<void3>',
    'router_path' => '<void3>',
    'link_title' => 'Science',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'menu-quick-navigation_science:<void3>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Events');
  t('News');
  t('Partners');
  t('Procurement');
  t('Science');

  return $menu_links;
}
