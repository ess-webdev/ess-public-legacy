<?php
/**
 * @file
 * no_front.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function no_front_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function no_front_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function no_front_image_default_styles() {
  $styles = array();

  // Exported image style: front_puff.
  $styles['front_puff'] = array(
    'label' => 'front_puff',
    'effects' => array(
      2 => array(
        'name' => 'manualcrop_auto_reuse',
        'data' => array(
          'style_priority' => array(
            0 => 'full-width_crop',
            1 => 'frontpage_crop',
          ),
          'apply_all_effects' => 0,
          'fallback_style' => '',
        ),
        'weight' => 0,
      ),
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 235,
          'height' => 150,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: frontpage_crop.
  $styles['frontpage_crop'] = array(
    'label' => 'Frontpage crop',
    'effects' => array(
      1 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 360,
          'height' => 287,
          'upscale' => 1,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'frontpage_crop',
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}
