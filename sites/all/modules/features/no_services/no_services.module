<?php

$host = variable_get('phonebook-sync-url', 'http://intranet.ess.se2.nod1.se');
define('PHONEBOOK_BASE_URL', $host . '/api/phonebook');
define('PHONEBOOK_LOGIN', 'admin');
define('PHONEBOOK_PASSWORD', '100%ess');

/**
 * Implements hook_menu()
 */
function no_services_menu() {
  $items['phonebook/getall'] = array(
    'title' => 'Get all phone book items',
    'page callback' => 'no_services_main',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['admin/config/phonebook'] = array(
    'title' => 'Phonebook sync settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('no_services_settings_form'),
    'access arguments' => array('administer content'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

function no_services_settings_form() {

  $form['phonebook-sync-url'] = array(
    '#type' => 'textfield',
    '#title' => 'Intranet URL',
    '#description' => 'The URL to the intranet from where you want to fetch the phonebook data. No trailing / !',
    '#default_value' => variable_get('phonebook-sync-url', 'loc.intranet.ess.com'),
    '#size' => 60,
    '#maxlength' => 255,
    '#weight' => 1,
    '#states' => array(
      'visible' => array(
        ':input[name="phonebook-sync-on-cron"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['phonebook-sync-nth-items'] = array(
    '#type' => 'textfield',
    '#title' => 'Users fetched in each batch',
    '#description' => 'The number of users that will be fetched in each batch.',
    '#default_value' => variable_get('phonebook-sync-nth-items', 5),
    '#size' => 5,
    '#maxlength' => 3,
    '#weight' => 2,
  );

  $form['phonebook-sync-on-cron'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable sync phonebook on cron',
    '#default_value' => variable_get('phonebook-sync-on-cron', 0),
    '#weight' => 0,
  );

  $options = array(
    900 => 'Every 15 minutes',
    1800 => 'Every 30 minutes',
    3600 => 'Every hour',
    10800 => 'Every 3 hours',
    21600 => 'Every 6 hours',
    43200 => 'Every 12 hours',
    86400 => 'Every day',
    172800 => 'Every 2nd day',
    604800 => 'Every week',
  );

  $form['phonebook-sync-cron-delay'] = array(
    '#type' => 'select',
    '#title' => 'How often should it fetch everything again?',
    '#description' => 'This is the time between each fetch. Remember that the system is fetching ALL entries in the phonebook on the intranet and not just the latest ones.',
    '#options' => $options,
    '#default_value' => variable_get('phonebook-sync-cron-delay', 86400),
    '#weight' => 3,
    '#states' => array(
      'visible' => array(
        ':input[name="phonebook-sync-on-cron"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['phonebook'] = array(
    '#type' => 'fieldset',
    '#title' => 'Sync manually!',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 4,

  );
  $form['phonebook']['phonebook-sync-now'] = array(
    '#type' => 'submit',
    '#value' => 'Sync phonebook now!',
    '#description' => 'NOTE: This task might take some time. Please do not click more than once!',
    '#submit' => array('no_services_main'),
  );
  $form['phonebook']['description'] = array(
    '#type' => 'item',
    '#description' => 'NOTE: This task might take some time. Please do not click more than once!',
  );

  return system_settings_form($form);
}



/**
 * Implements hook_cron()
 */
function no_services_cron() {
  // Only run this if the variable is set to 1
  if (variable_get('phonebook-sync-on-cron', 0) == 1) {
    $last_fetch = variable_get('phonebook_last_fetched', FALSE);

    //variable_set('phonebook_tree', NULL);
    //$last_fetch = FALSE; // for debugging only. Make sure to comment out when done!!

    // Check if some time has gone after previous complete fetch before doing a new.
    // Default to one day between each fetch.
    if ($last_fetch == FALSE) {
      no_services_main(TRUE);
    }
    else {
      $next_time = $last_fetch + variable_get('phonebook-sync-cron-delay', 86400);
      if ($next_time < time()) {
        no_services_main(TRUE);
      }
      else {
        no_services_sync_message("Next scheduled start of fetching phonebook starts: " . date('Y-m-d H:i:s', $next_time), $cron);
      }
    }
  }
}

/**
 * Login to Drupal
 */
function no_services_phonebook_login() {

  $data = array(
    'username' => PHONEBOOK_LOGIN,
    'password' => PHONEBOOK_PASSWORD,
  );
  $data = http_build_query($data, '', '&');
  $headers = array();
  $options = array(
    'headers' => array(
      'Accept' => 'application/json',
    ),
    'method' => 'POST',
    'data' => $data
  );
  $response = drupal_http_request(PHONEBOOK_BASE_URL . '/user/login', $options);
  if ($response->code == 200) {
    $data = json_decode($response->data);
    $options['headers']['Cookie'] = $data->session_name . '=' . $data->sessid;
    return $options;
  }
  return FALSE;
}

function _no_services_calculate_tree($tree, $cron) {
  foreach ($tree as $dir_uuid => $directorate) {
    $directorates[$dir_uuid] = $dir_uuid;
    foreach ($directorate['children'] as $div_uuid => $division) {
      $divisions[$div_uuid] = $div_uuid;
      foreach ($division['children'] as $user_uuid => $user) {
        $users[$user_uuid] = $user_uuid;
      }
    }
  }
 
  $nth_users = !empty($users) ? count($users) : 0;

  no_services_sync_message('There is: '
      . count($directorates) . ' directorates, '
      . count($divisions) . ' divisions and '
      . $nth_users . ' users left in the tree to be fetched.', $cron);
}

/**
 * Main Page callback
 */
function no_services_main($cron = FALSE) {
  if (is_array($cron)) {
    $cron = FALSE;
  }

  $dir_fetch_limit = 1;
  $div_fetch_limit = 1;
  $user_fetch_limit = variable_get('phonebook-sync-nth-items', 5);

  $options = no_services_phonebook_login();
  // Check if login was successful
  if ($options != FALSE) {
    // If the phonebook variable is not set yet or empty, get it again.
    $tree = variable_get('phonebook_tree', NULL);
    if ($tree == NULL || empty($tree) || (isset($_GET['phonedebug']) &&  $_GET['phonedebug'] == true)) {
      $tree = _no_service_get_tree_structure($options);
      no_services_sync_message('Fetching tree from intranet', $cron);
    }

    if ($tree != FALSE) {
      $directorates = array_keys($tree);
      for ($i = 0; count($directorates) && $i < $dir_fetch_limit; $i++) {
        $dir_uuid = $directorates[$i];
        // Check if the directorate is alreade fetched.
        if (empty($tree[$dir_uuid]['fetched'])) {
          // fetch and save the directorate term:
          $term = _no_services_get_entity_data($tree[$dir_uuid]['type'], $dir_uuid, $options);
          // update term if exist or create it locally
          $term_id = _no_services_update_directorate_term($term, $cron);

          // set fetched to true
          $tree[$dir_uuid]['fetched'] = TRUE;
        }
        else {
          // get term id from uuid
          $term_id = db_query("SELECT tid FROM {taxonomy_term_data} WHERE uuid = :uuid", array(':uuid' => $dir_uuid))->fetchField();
        }
        // get the directorate object
        $directorate = $tree[$dir_uuid];
        // get all the divions uuids
        $divisions = array_keys($directorate['children']);
        // iterate through them
        for ($j = 0; count($divisions) && $j < $div_fetch_limit; $j++) {
          $div_uuid = $divisions[$j];
          // Check if the division is alreade fetched.
          if (empty($tree[$dir_uuid]['children'][$div_uuid]['fetched'])) {
            // fetch and save the division term:
            $group_node = _no_services_get_entity_data($tree[$dir_uuid]['children'][$div_uuid]['type'], $div_uuid, $options);
            $div_term_id = _no_services_update_division_term($group_node, $term_id, $cron);

            // set fetched to true
            $tree[$dir_uuid]['children'][$div_uuid]['fetched'] = TRUE;
          }
          else {
            // get term id from uuid
            $div_term_id = db_query("SELECT tid FROM {taxonomy_term_data} WHERE uuid = :uuid", array(':uuid' => $div_uuid))->fetchField();
          }
          $division = $tree[$dir_uuid]['children'][$div_uuid];
          if (!empty($division['children'])) {
            $users = array_keys($division['children']);
            for($k = 0; count($tree[$dir_uuid]['children'][$div_uuid]['children']) && $k < $user_fetch_limit; $k++) {
              $user_uuid = $users[$k];
              $user = array_shift($tree[$dir_uuid]['children'][$div_uuid]['children']);
              $user_entity = _no_services_get_entity_data($user['type'], $user_uuid, $options);
              // If user 1 or if the user already has been save to a node.
              if ($user_entity->uid == 1) {
                continue;
              }
              $user_nid = _no_service_update_person_node($user_entity, $div_term_id, $cron);
            }
          }
          if (!count($tree[$dir_uuid]['children'][$div_uuid]['children'])) {
            // Remove the division from the tree.
            unset($tree[$dir_uuid]['children'][$div_uuid]);
          }
        }
        if (!count($tree[$dir_uuid]['children'])) {
          // Remove the directorate from the tree.
          unset($tree[$dir_uuid]);
        }
      }
      if (empty($tree)) {
        // There is nothing more to fetch right now
        variable_set('phonebook_last_fetched', time());
        variable_set('phonebook_tree', $tree);
        $next_fetch = date('Y-m-d H:i', variable_get('phonebook_last_fetched', time()) + variable_get('phonebook-sync-cron-delay', 86400));
        no_services_sync_message('Everything is fetched now. Next scheduled start of fetching starts: ' . $next_fetch, $cron);
      }
      else {
        // There is more to fetch
        variable_set('phonebook_tree', $tree);
        _no_services_calculate_tree($tree, $cron);
        no_services_sync_message('Updated tree variable for next cron.', $cron);
      }
    }
  }
  return;
}

/**
 * Get complete tree structure from web service.
 */
function _no_service_get_tree_structure($options) {
  // Get tree structure
  $options['method'] = 'GET';
  $response = drupal_http_request(PHONEBOOK_BASE_URL . '/tree', $options);
  if ($response->code == 200) {
    $data = drupal_json_decode($response->data);
    drupal_set_message('Get tree structure', 'status');
    return $data;
  }
  return FALSE;
}

/**
 * Get complete tree structure from web service.
 */
function _no_services_get_entity_data($entity_type, $uuid, $options) {
  // Get tree structure
  $options['method'] = 'GET';
  $response = drupal_http_request(PHONEBOOK_BASE_URL . '/' . $entity_type . '/' . $uuid, $options);
  if ($response->code == 200) {
    $data = json_decode($response->data);
    return $data;
  }
  return FALSE;
}

/**
 * Create a taxonomy term and return the tid.
 */
function _no_services_update_directorate_term($entity, $cron) {
  $destination_vocabulary = taxonomy_vocabulary_machine_name_load('no_directorate');

  if ($term = entity_uuid_load('taxonomy_term', array($entity->uuid))) {
    // Term exists
    $term = reset($term);
    $term->name = $entity->name;
    taxonomy_term_save($term);
    no_services_sync_message('Updated directorate term: ' . $term->uuid, $cron);
    return $term->tid;
  }
  else {
    // Term doesnt exist
    $term = new stdClass();
    $term->name = $entity->name;
    $term->vid = $destination_vocabulary->vid;
    $term->vocabulary_machine_name = $destination_vocabulary->machine_name;
    $term->uuid = $entity->uuid;
    taxonomy_term_save($term);
    no_services_sync_message('Created new directorate term: ' . $term->uuid, $cron);
    return $term->tid;
  }
}

/**
 * Create a taxonomy term and return the tid.
 */
function _no_services_update_division_term($entity, $referred_tid, $cron) {
  $destination_vocabulary = taxonomy_vocabulary_machine_name_load('no_division');

  if ($term = entity_uuid_load('taxonomy_term', array($entity->uuid))) {
    // Term exists
    $term = reset($term);
    $term->name = $entity->title;
    $term->field_no_directorate[LANGUAGE_NONE][0]['tid'] = !empty($referred_tid) ? $referred_tid : $term->field_no_directorate[LANGUAGE_NONE][0]['tid'];
    taxonomy_term_save($term);
    no_services_sync_message('Updated division term: ' . $term->uuid, $cron);
    return $term->tid;
  }
  else {
    // Term doesnt exist
    $term = new stdClass();
    $term->name = $entity->title;
    $term->vid = $destination_vocabulary->vid;
    $term->vocabulary_machine_name = $destination_vocabulary->machine_name;
    $term->uuid = $entity->uuid;
    $term->field_no_directorate[LANGUAGE_NONE][0]['tid'] = !empty($referred_tid) ? $referred_tid : $term->field_no_directorate[LANGUAGE_NONE][0]['tid'];
    taxonomy_term_save($term);
    no_services_sync_message('Created division term: ' . $term->uuid, $cron);
    return $term->tid;
  }
}

/**
 * Create a taxonomy term and return the tid.
 */
function _no_service_update_person_node($entity, $referred_tid, $cron) {

  if ($node = entity_uuid_load('node', array($entity->uuid))) {
    // Node exists
    $node = reset($node);
    $node->title = $entity->name;

    $node->field_no_division[LANGUAGE_NONE][0]['tid'] = !empty($referred_tid) ? $referred_tid : NULL;

    $node->field_no_phonebook_entry_fname[LANGUAGE_NONE][0]
        = !empty($entity->field_ns_user_firstname->und[0]) ? (Array)$entity->field_ns_user_firstname->und[0] : NULL;

    $node->field_no_phonebook_entry_lname[LANGUAGE_NONE][0]
        = !empty($entity->field_ns_user_lastname->und[0]) ? (Array)$entity->field_ns_user_lastname->und[0] : NULL;

    $node->field_no_phonebook_entry_phone[LANGUAGE_NONE][0]
        = !empty($entity->field_ns_user_phone->und[0]) ? (Array)$entity->field_ns_user_phone->und[0] : NULL;

    $node->field_no_phonebook_entry_mobile[LANGUAGE_NONE][0]
        = !empty($entity->field_ns_user_mobile->und[0]) ? (Array)$entity->field_ns_user_mobile->und[0] : NULL;

    $node->field_no_phonebook_entry_email[LANGUAGE_NONE][0]['email'] = $entity->mail;

    node_save($node);
    no_services_sync_message('Updated person node: ' . $node->uuid, $cron);
    return $node->nid;
  }
  else {
    // Node doesnt exist
    $node = new stdClass();
    $node->title = $entity->name;
    $node->status = 1;
    $node->language = 'und';
    $node->type = 'no_phonebook_entry';
    $node->field_no_division[LANGUAGE_NONE][0]['tid'] = !empty($referred_tid) ? $referred_tid : NULL;

    $node->field_no_phonebook_entry_fname[LANGUAGE_NONE][0]
        = !empty($entity->field_ns_user_firstname->und[0]) ? (Array)$entity->field_ns_user_firstname->und[0] : NULL;

    $node->field_no_phonebook_entry_lname[LANGUAGE_NONE][0]
        = !empty($entity->field_ns_user_lastname->und[0]) ? (Array)$entity->field_ns_user_lastname->und[0] : NULL;

    $node->field_no_phonebook_entry_phone[LANGUAGE_NONE][0]
        = !empty($entity->field_ns_user_phone->und[0]) ? (Array)$entity->field_ns_user_phone->und[0] : NULL;

    $node->field_no_phonebook_entry_mobile[LANGUAGE_NONE][0]
        = !empty($entity->field_ns_user_mobile->und[0]) ? (Array)$entity->field_ns_user_mobile->und[0] : NULL;

    $node->field_no_phonebook_entry_email[LANGUAGE_NONE][0]['email'] = $entity->mail;

    $node->uuid = $entity->uuid;
    node_save($node);
    no_services_sync_message('Created person node: ' . $node->uuid, $cron);
    return $node->nid;
  }
}

function no_services_sync_message($message, $cron) {
  if ($cron) {
    print $message . "\n";
  }
  else {
    drupal_set_message($message, 'status');
  }
}