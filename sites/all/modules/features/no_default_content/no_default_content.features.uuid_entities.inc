<?php
/**
 * @file
 * no_default_content.features.uuid_entities.inc
 */

/**
 * Implements hook_uuid_default_entities().
 */
function no_default_content_uuid_default_entities() {
  $entities = array();

  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'node',
      'uri' => 'node/8e879589-6381-51e4-89cd-4fbb3bd06075',
      'cause' => FALSE,
    ),
    'comment' => 0,
    'field_no_image' => array(),
    'field_no_lead' => array(),
    'field_no_promo' => array(),
    'field_no_slideshow' => array(),
    'group_group' => array(
      'und' => array(
        0 => array(
          'value' => 0,
        ),
      ),
    ),
    'language' => 'und',
    'log' => '',
    'metatags' => array(
      'und' => array(
        'title' => array(
          'value' => '[current-page:title] | [site:name]',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => FALSE,
      'alias' => 'home',
    ),
    'promote' => 0,
    'status' => 1,
    'sticky' => 0,
    'title' => 'Home',
    'tnid' => 0,
    'translate' => 0,
    'type' => 'no_section',
    'uid' => 1,
    'uuid' => '8e879589-6381-51e4-89cd-4fbb3bd06075',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'file',
      'uri' => 'file/43de93cf-0277-9374-ed77-6465b6329f21',
      'cause' => 'node/dbbb24ff-e7d9-6a84-6118-1d03b9a14a94',
    ),
    'field_no_caption' => array(),
    'filemime' => 'image/jpeg',
    'filename' => 'ess_poster_natural_cropped_0.jpg',
    'filesize' => 115511,
    'status' => 1,
    'type' => 'image',
    'uid' => '639fcfba-4934-90c4-1964-a62bf7b7fea0',
    'uri' => 'public://ess_poster_natural_cropped_0_0.jpg',
    'uuid' => '43de93cf-0277-9374-ed77-6465b6329f21',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'user',
      'uri' => 'user/639fcfba-4934-90c4-1964-a62bf7b7fea0',
      'cause' => 'node/dbbb24ff-e7d9-6a84-6118-1d03b9a14a94',
    ),
    'init' => 'allen.weeks@esss.se',
    'language' => '',
    'mail' => 'allen.weeks@esss.se',
    'name' => 'allen.weeks',
    'og_user_group_ref' => array(
      'und' => array(
        0 => array(
          'target_id' => '2d6eb5d0-a34a-c994-7db1-03b4ed843d06',
        ),
        1 => array(
          'target_id' => 'cef5e299-75e5-c694-e1a1-b11784f0683f',
        ),
        2 => array(
          'target_id' => '7d1b0ffc-454c-0064-d19e-bb3ba356a346',
        ),
        3 => array(
          'target_id' => '445760a9-392f-55a4-7917-7736dcbc23b0',
        ),
        4 => array(
          'target_id' => '99b463d3-c88a-ba64-adcb-8f411d127feb',
        ),
        5 => array(
          'target_id' => 'b9d98984-f878-0ef4-fdd3-6f7e8bd858d6',
        ),
        6 => array(
          'target_id' => '3501aea5-a8f2-bcb4-29ad-2d61318068a7',
        ),
        7 => array(
          'target_id' => '693587ce-77d1-8204-6158-6e2fadbe5dda',
        ),
        8 => array(
          'target_id' => '6ab8adf1-5ba9-4994-91f6-2623f9ce6331',
        ),
        9 => array(
          'target_id' => 'd22f068a-5795-fb74-e984-9d42919f2ca7',
        ),
        10 => array(
          'target_id' => '32f95a8f-f361-64f4-91c3-1179bb6d31ee',
        ),
        11 => array(
          'target_id' => '52ee7ad8-e70e-3264-6d54-a360d60a290e',
        ),
        12 => array(
          'target_id' => 'c20c0e2e-7e95-99c4-09a9-3c8af74a2df5',
        ),
        13 => array(
          'target_id' => '6bf2fd1b-c02d-2994-d10e-88fda862ac9b',
        ),
      ),
    ),
    'pass' => '$S$DYUfrqVfw6Yw0pdxn7jJFN7H7klwr7buumuxx1sGs9FAN/iFldhl',
    'picture' => 0,
    'roles' => array(
      2 => 'authenticated user',
      4 => 'Section editor',
      5 => 'Site administrator',
    ),
    'signature' => '',
    'signature_format' => 'plain_text',
    'status' => 1,
    'theme' => '',
    'timezone' => NULL,
    'uuid' => '639fcfba-4934-90c4-1964-a62bf7b7fea0',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'node',
      'uri' => 'node/dbbb24ff-e7d9-6a84-6118-1d03b9a14a94',
      'cause' => FALSE,
    ),
    'comment' => 0,
    'field_no_image' => array(
      'und' => array(
        0 => array(
          'alt' => NULL,
          'fid' => '43de93cf-0277-9374-ed77-6465b6329f21',
          'field_no_caption' => array(),
          'filemime' => 'image/jpeg',
          'filename' => 'ess_poster_natural_cropped_0.jpg',
          'filesize' => 115511,
          'height' => 231,
          'status' => 1,
          'title' => NULL,
          'type' => 'image',
          'uid' => '639fcfba-4934-90c4-1964-a62bf7b7fea0',
          'uri' => 'public://ess_poster_natural_cropped_0_0.jpg',
          'uuid' => '43de93cf-0277-9374-ed77-6465b6329f21',
          'width' => 918,
        ),
      ),
    ),
    'field_no_lead' => array(
      'und' => array(
        0 => array(
          'format' => 'full_html',
          'value' => '<p>Seventeen European countries are working together to build a leading research facility of unparalleled power and scientific performance for materials and life science with neutrons &ndash; the European Spallation Source.&nbsp;</p><div>&nbsp;</div><p>Right now, hundreds of scientists and engineers from all over the world are working on the design and planning of the ESS facility. This modern research infrastructure will open new scientific opportunities for scientists from Europe and all over the world, providing unrivaled experimental opportunities for thousands of scientists every year.</p><p>&nbsp;</p><p>On these pages you will find general information about all aspects of the ESS project, as well as news stories and feature articles, facts &amp; figures, documentation and contact details.</p><div>&nbsp;</div>',
        ),
      ),
    ),
    'field_no_promo' => array(
      'und' => array(
        0 => array(
          'target_id' => '138f4682-93ba-7d54-c9ec-bb65c95abb06',
        ),
        1 => array(
          'target_id' => '982663ff-2f43-96a4-3d08-417b06bc6cc8',
        ),
      ),
    ),
    'field_no_slideshow' => array(),
    'group_group' => array(
      'und' => array(
        0 => array(
          'value' => 0,
        ),
      ),
    ),
    'language' => 'und',
    'log' => '',
    'path' => array(
      'pathauto' => FALSE,
      'alias' => 'about-ess',
    ),
    'promote' => 0,
    'status' => 0,
    'sticky' => 0,
    'title' => 'About ESS',
    'tnid' => 0,
    'translate' => 0,
    'type' => 'no_section',
    'uid' => 1,
    'uuid' => 'dbbb24ff-e7d9-6a84-6118-1d03b9a14a94',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'file',
      'uri' => 'file/98aac21b-5d3b-6a84-21ce-d8e6e9c326e6',
      'cause' => 'node/2d6eb5d0-a34a-c994-7db1-03b4ed843d06',
    ),
    'field_no_caption' => array(),
    'filemime' => 'image/jpeg',
    'filename' => 'science_banner.jpeg',
    'filesize' => 396592,
    'status' => 1,
    'type' => 'image',
    'uid' => '93a91c77-e7d1-a814-656a-4645ca657172',
    'uri' => 'public://science_banner.jpeg',
    'uuid' => '98aac21b-5d3b-6a84-21ce-d8e6e9c326e6',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'user',
      'uri' => 'user/93a91c77-e7d1-a814-656a-4645ca657172',
      'cause' => 'node/2d6eb5d0-a34a-c994-7db1-03b4ed843d06',
    ),
    'init' => 'sindra.petersson@esss.se',
    'language' => '',
    'mail' => 'sindra.petersson@esss.se',
    'name' => 'Sindra Petersson Årsköld',
    'og_user_group_ref' => array(
      'und' => array(
        0 => array(
          'target_id' => '2d6eb5d0-a34a-c994-7db1-03b4ed843d06',
        ),
        1 => array(
          'target_id' => 'cef5e299-75e5-c694-e1a1-b11784f0683f',
        ),
        2 => array(
          'target_id' => '7d1b0ffc-454c-0064-d19e-bb3ba356a346',
        ),
        3 => array(
          'target_id' => '445760a9-392f-55a4-7917-7736dcbc23b0',
        ),
        4 => array(
          'target_id' => '99b463d3-c88a-ba64-adcb-8f411d127feb',
        ),
        5 => array(
          'target_id' => 'b9d98984-f878-0ef4-fdd3-6f7e8bd858d6',
        ),
        6 => array(
          'target_id' => 'dbbb24ff-e7d9-6a84-6118-1d03b9a14a94',
        ),
      ),
    ),
    'pass' => '$S$Dc.PEd.q.Obb5lGJ7CAJ2ItzyAd.cqlBCk/amtEGCxxC14wG.mP8',
    'picture' => 0,
    'roles' => array(
      2 => 'authenticated user',
      5 => 'Site administrator',
    ),
    'signature' => '',
    'signature_format' => 'no_text_editor',
    'status' => 1,
    'theme' => '',
    'timezone' => NULL,
    'uuid' => '93a91c77-e7d1-a814-656a-4645ca657172',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'node',
      'uri' => 'node/2d6eb5d0-a34a-c994-7db1-03b4ed843d06',
      'cause' => FALSE,
    ),
    'comment' => 0,
    'field_no_image' => array(
      'und' => array(
        0 => array(
          'alt' => NULL,
          'fid' => '98aac21b-5d3b-6a84-21ce-d8e6e9c326e6',
          'field_no_caption' => array(),
          'filemime' => 'image/jpeg',
          'filename' => 'science_banner.jpeg',
          'filesize' => 396592,
          'height' => 254,
          'status' => 1,
          'title' => NULL,
          'type' => 'image',
          'uid' => '93a91c77-e7d1-a814-656a-4645ca657172',
          'uri' => 'public://science_banner.jpeg',
          'uuid' => '98aac21b-5d3b-6a84-21ce-d8e6e9c326e6',
          'width' => 1843,
        ),
      ),
    ),
    'field_no_lead' => array(
      'und' => array(
        0 => array(
          'format' => 'full_html',
          'value' => '<p>On these pages, you will find examples of <a href="http://europeanspallationsource.se/science-using-neutrons" target="_self">science using neutrons</a> from a wide range of different research areas, and you\'ll get an idea of what will be made possible by <a href="http://europeanspallationsource.se/unique-capabilities-ess" target="_self">the unique capabilities of ESS</a>.</p><div><hr><span style="font-size:14px;"><span style="color:#323333;">You can read about the different </span><a href="/instruments-and-support-facilities">neutron-scattering techniques and instruments</a><span style="color:#323333;"> that are being developed at ESS, and follow the progress being made in </span><a href="/node/244">neutron technologies</a><span style="color:#323333;"> and </span><a href="/data-management-and-software">data management</a><span style="color:#323333;"> systems. </span></span></div><div>&nbsp;</div><div><span style="font-size:14px;"><span style="color:#323333;">You can learn <a href="http://europeanspallationsource.se/instruments2013">how the ESS instruments are selected</a>, including who is on the advisory panels, and follow the <a href="http://europeanspallationsource.se/constructing-instruments-and-science-support">instruments that have entered the first phase of construction</a>.</span></span></div><div>&nbsp;</div><div><span style="font-size:14px;"><span style="color:#323333;">There are many ways to </span><a href="/get-involved">get involved</a><span style="color:#323333;"> in the scientific and technological development of ESS, and we are keen to collaborate with both the academic and the </span><a href="/industrial-research-development">industrial research</a><span style="color:#323333;"> sector. </span></span></div><div>&nbsp;</div><div><span style="font-size:14px;"><span style="color:#323333;">Members of staff present themselves on the </span><a href="/science-organisation-and-staff">staff pages</a><span style="color:#323333;">, and our European partners are presented on the </span><a href="/kind-partnerships-and-collaborations">partners page</a><span style="color:#323333;">. Stay up-to-date through the events listing on the right-hand side and the news flow below, and feel free to attend our </span><a href="/seminars-and-workshops">seminars<span style="color:#323333;"> and </span>workshops<span style="color:#323333;">.</span></a></span></div><div>&nbsp;</div>',
        ),
      ),
    ),
    'field_no_promo' => array(
      'und' => array(
        0 => array(
          'target_id' => 'ef61287b-866a-b3d4-4163-5c26d012ba66',
        ),
        1 => array(
          'target_id' => 'b4c2595e-bdb7-d544-3136-e0f52972172c',
        ),
        2 => array(
          'target_id' => 'd30cba8c-57c3-5364-dde0-81d99fb514ef',
        ),
        3 => array(
          'target_id' => '7bb5b9be-5c3d-a204-9536-79c7022fe209',
        ),
      ),
    ),
    'field_no_slideshow' => array(),
    'group_group' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'language' => 'und',
    'log' => '',
    'path' => array(
      'pathauto' => FALSE,
      'alias' => 'science-and-instruments',
    ),
    'promote' => 0,
    'status' => 1,
    'sticky' => 0,
    'title' => 'Science and Instruments',
    'tnid' => 0,
    'translate' => 0,
    'type' => 'no_section',
    'uid' => '93a91c77-e7d1-a814-656a-4645ca657172',
    'uuid' => '2d6eb5d0-a34a-c994-7db1-03b4ed843d06',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'node',
      'uri' => 'node/cef5e299-75e5-c694-e1a1-b11784f0683f',
      'cause' => FALSE,
    ),
    'comment' => 0,
    'field_no_image' => array(),
    'field_no_lead' => array(
      'und' => array(
        0 => array(
          'format' => 'full_html',
          'value' => '<p>[[{"type":"media","view_mode":"media_original","fid":"1323","attributes":{"alt":"","class":"media-image  media-image-right","height":"381","style":"width: 259px; height: 275px; float: right;","width":"329"}}]]The neutrons that scientists need to study materials and molecules are produced in the target station. It is here that the spallation process takes place when protons from the accelerator hit the target, <a href="https://europeanspallationsource.se/article/building-heart-ess-spain" target="_blank">a 4-tonne helium-cooled tungsten wheel</a>. The design of the target has a direct impact on the number of neutrons that can be generated, and is therefore of utmost importance for the future scientific capabilities of the ESS facility.</p><p>&nbsp;</p><p>&nbsp;</p>',
        ),
      ),
    ),
    'field_no_promo' => array(),
    'field_no_slideshow' => array(),
    'group_group' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'language' => 'und',
    'log' => '',
    'path' => array(
      'pathauto' => FALSE,
      'alias' => 'target',
    ),
    'promote' => 0,
    'status' => 1,
    'sticky' => 0,
    'title' => 'Target',
    'tnid' => 0,
    'translate' => 0,
    'type' => 'no_section',
    'uid' => 1,
    'uuid' => 'cef5e299-75e5-c694-e1a1-b11784f0683f',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'file',
      'uri' => 'file/4cfc3549-1153-f724-c9d3-1175d82e185e',
      'cause' => 'node/7d1b0ffc-454c-0064-d19e-bb3ba356a346',
    ),
    'field_no_caption' => array(),
    'filemime' => 'image/jpeg',
    'filename' => 'op_linac_c.jpg',
    'filesize' => 163737,
    'status' => 1,
    'type' => 'image',
    'uid' => '97201001-f331-de04-7977-eda0b83835bc',
    'uri' => 'public://op_linac_c_0.jpg',
    'uuid' => '4cfc3549-1153-f724-c9d3-1175d82e185e',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'user',
      'uri' => 'user/97201001-f331-de04-7977-eda0b83835bc',
      'cause' => 'node/7d1b0ffc-454c-0064-d19e-bb3ba356a346',
    ),
    'init' => 'eugene.tanke@esss.se',
    'language' => '',
    'mail' => 'eugene.tanke@esss.se',
    'name' => 'eugenetanke',
    'og_user_group_ref' => array(
      'und' => array(
        0 => array(
          'target_id' => '7d1b0ffc-454c-0064-d19e-bb3ba356a346',
        ),
        1 => array(
          'target_id' => '52ee7ad8-e70e-3264-6d54-a360d60a290e',
        ),
        2 => array(
          'target_id' => 'c20c0e2e-7e95-99c4-09a9-3c8af74a2df5',
        ),
      ),
    ),
    'pass' => '$S$DxAx8SRk7nKMtktxSMbXjoPkLx9qXYhX9UL.0hagkfolLZa6ToBU',
    'picture' => 0,
    'roles' => array(
      2 => 'authenticated user',
      4 => 'Section editor',
    ),
    'signature' => '',
    'signature_format' => 'no_text_editor',
    'status' => 1,
    'theme' => '',
    'timezone' => NULL,
    'uuid' => '97201001-f331-de04-7977-eda0b83835bc',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'node',
      'uri' => 'node/7d1b0ffc-454c-0064-d19e-bb3ba356a346',
      'cause' => FALSE,
    ),
    'comment' => 0,
    'field_no_image' => array(
      'und' => array(
        0 => array(
          'alt' => NULL,
          'fid' => '4cfc3549-1153-f724-c9d3-1175d82e185e',
          'field_no_caption' => array(),
          'filemime' => 'image/jpeg',
          'filename' => 'op_linac_c.jpg',
          'filesize' => 163737,
          'height' => 472,
          'status' => 1,
          'title' => NULL,
          'type' => 'image',
          'uid' => '97201001-f331-de04-7977-eda0b83835bc',
          'uri' => 'public://op_linac_c_0.jpg',
          'uuid' => '4cfc3549-1153-f724-c9d3-1175d82e185e',
          'width' => 2840,
        ),
      ),
    ),
    'field_no_lead' => array(
      'und' => array(
        0 => array(
          'format' => 'full_html',
          'value' => '<div>The ESS accelerator high level requirements are to provide a 2.86 ms long proton pulse at 2 GeV at repetition rate of 14 Hz. This represents 5 MW of average beam power with a 4% duty cycle on target.<hr><div><span style="color:#333333;"><span style="font-size:14px;">The ion source produces a proton beam that is transported through a Low Energy Beam Transport (LEBT) section to the Radio Frequency Quadrupole (RFQ) where it is bunched and accelerated up to 3.6 MeV. In the Medium Energy Beam Transport (MEBT) section the transverse and longitudinal beam characteristics are diagnosed and optimized for further acceleration in the Drift Tube Linac (DTL). The first superconducting section consists of 26 double-spoke cavities (SPK) with a geometric beta value of 0.50. The spoke-cavities are followed by 36 Medium Beta Linac (MBL) cavities with β = 0.67 and 84 High Beta Linac (HBL) elliptical cavities, with β = 0.86. After acceleration the beam is transported to the target through the High Energy Beam Transport (HEBT) section.</span></span></div></div><p>&nbsp;</p>',
        ),
      ),
    ),
    'field_no_promo' => array(
      'und' => array(
        0 => array(
          'target_id' => 'c5b0db68-b37e-6254-1151-7af11ec86586',
        ),
        1 => array(
          'target_id' => 'ca394da0-e10e-fe54-0957-a6026b370ad9',
        ),
        2 => array(
          'target_id' => 'ac41d7f8-87bb-9f84-d9ea-4a30e04d0f9a',
        ),
        3 => array(
          'target_id' => '2fb5df7d-5062-ba34-31f9-820504ef6574',
        ),
      ),
    ),
    'field_no_slideshow' => array(),
    'group_group' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'language' => 'und',
    'log' => '',
    'path' => array(
      'pathauto' => FALSE,
      'alias' => 'accelerator',
    ),
    'promote' => 0,
    'status' => 1,
    'sticky' => 0,
    'title' => 'Accelerator',
    'tnid' => 0,
    'translate' => 0,
    'type' => 'no_section',
    'uid' => '97201001-f331-de04-7977-eda0b83835bc',
    'uuid' => '7d1b0ffc-454c-0064-d19e-bb3ba356a346',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'file',
      'uri' => 'file/c62942e0-e51b-d5d4-0149-f461f169d002',
      'cause' => 'node/445760a9-392f-55a4-7917-7736dcbc23b0',
    ),
    'field_no_caption' => array(),
    'filemime' => 'image/jpeg',
    'filename' => 'stone_banner1000.jpg',
    'filesize' => 24987,
    'status' => 1,
    'type' => 'image',
    'uid' => '97a7455b-3967-9134-51f6-845ec332b533',
    'uri' => 'public://stone_banner1000.jpg',
    'uuid' => 'c62942e0-e51b-d5d4-0149-f461f169d002',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'user',
      'uri' => 'user/97a7455b-3967-9134-51f6-845ec332b533',
      'cause' => 'node/445760a9-392f-55a4-7917-7736dcbc23b0',
    ),
    'init' => 'James.Tierney@esss.se',
    'language' => '',
    'mail' => 'James.Tierney@esss.se',
    'name' => 'jamestierney',
    'og_user_group_ref' => array(
      'und' => array(
        0 => array(
          'target_id' => '2d6eb5d0-a34a-c994-7db1-03b4ed843d06',
        ),
        1 => array(
          'target_id' => 'cef5e299-75e5-c694-e1a1-b11784f0683f',
        ),
        2 => array(
          'target_id' => '7d1b0ffc-454c-0064-d19e-bb3ba356a346',
        ),
        3 => array(
          'target_id' => '445760a9-392f-55a4-7917-7736dcbc23b0',
        ),
        4 => array(
          'target_id' => '99b463d3-c88a-ba64-adcb-8f411d127feb',
        ),
        5 => array(
          'target_id' => 'b9d98984-f878-0ef4-fdd3-6f7e8bd858d6',
        ),
        6 => array(
          'target_id' => '3501aea5-a8f2-bcb4-29ad-2d61318068a7',
        ),
        7 => array(
          'target_id' => '693587ce-77d1-8204-6158-6e2fadbe5dda',
        ),
        8 => array(
          'target_id' => '6ab8adf1-5ba9-4994-91f6-2623f9ce6331',
        ),
        9 => array(
          'target_id' => 'd22f068a-5795-fb74-e984-9d42919f2ca7',
        ),
        10 => array(
          'target_id' => '32f95a8f-f361-64f4-91c3-1179bb6d31ee',
        ),
        11 => array(
          'target_id' => '52ee7ad8-e70e-3264-6d54-a360d60a290e',
        ),
        12 => array(
          'target_id' => 'c20c0e2e-7e95-99c4-09a9-3c8af74a2df5',
        ),
        13 => array(
          'target_id' => '6bf2fd1b-c02d-2994-d10e-88fda862ac9b',
        ),
      ),
    ),
    'pass' => '$S$DG11m22qNlR7wA1E3EZQU0hxvgtDyFXjKD4AUY0SUjIKF3M1NxiP',
    'picture' => 0,
    'roles' => array(
      2 => 'authenticated user',
      9 => 'Developer',
    ),
    'signature' => '',
    'signature_format' => 'no_text_editor',
    'status' => 1,
    'theme' => '',
    'timezone' => NULL,
    'uuid' => '97a7455b-3967-9134-51f6-845ec332b533',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'node',
      'uri' => 'node/445760a9-392f-55a4-7917-7736dcbc23b0',
      'cause' => FALSE,
    ),
    'comment' => 0,
    'field_no_image' => array(
      'und' => array(
        0 => array(
          'alt' => NULL,
          'fid' => 'c62942e0-e51b-d5d4-0149-f461f169d002',
          'field_no_caption' => array(),
          'filemime' => 'image/jpeg',
          'filename' => 'stone_banner1000.jpg',
          'filesize' => 24987,
          'height' => 263,
          'status' => 1,
          'title' => NULL,
          'type' => 'image',
          'uid' => '97a7455b-3967-9134-51f6-845ec332b533',
          'uri' => 'public://stone_banner1000.jpg',
          'uuid' => 'c62942e0-e51b-d5d4-0149-f461f169d002',
          'width' => 1000,
        ),
      ),
    ),
    'field_no_lead' => array(
      'und' => array(
        0 => array(
          'format' => 'full_html',
          'value' => '<p>Incorporated into the rural landscape of southern Sweden, the construction of the various ESS buildings and facilities comprises one of the largest active infrastructure projects in Europe.</p><hr><div><span style="color:#323333;"><span style="font-size:14px;">The creative vision of the building envelope and grounds, developed by the Henning Larsen Architects-led design consortium, will be coupled to the innovative technical design of ESS’s vast array of scientific machinery. The technical design work already represents one of the largest and broadest collaborations in Europe.</span></span></div><div>&nbsp;</div><div><table align="right" border="1" cellpadding="1" cellspacing="1" style="width: 250px;"><tbody><tr><td style="text-align: right;"><a href="http://europeanspallationsource.se/site-weekly-updates" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 13px; color: rgb(0, 148, 202); font-family: TitilliumTextMedium, helvetica, sans-serif;" target="_self"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; outline: 0px; font-size: 12px;">Click here to follow weekly developments on the construction site, see the site plan, and view high-resolution aerial photos</span></a><a href="http://europeanspallationsource.se/site-weekly-updates" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 13px; color: rgb(0, 148, 202); font-family: TitilliumTextMedium, helvetica, sans-serif;" target="_self"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; outline: 0px; font-size: 12px;">.</span></a></td></tr></tbody></table><span style="color:#323333;"><span style="font-size:14px;">The facility is expected to deliver its first neutrons in 2019 and be a major driver for innovation in science and industry in Europe and the region for generations.</span></span></div><div>&nbsp;</div><div><span style="color: rgb(50, 51, 51); font-size: 14px;">This area of the website has information on&nbsp;the global&nbsp;</span><a href="http://europeanspallationsource.se/ess-collaborations" style="font-size: 14px;" target="_self">scientific collaborations</a><span style="color: rgb(50, 51, 51); font-size: 14px;">&nbsp;that have brought the ESS design process to the Construction Phase;&nbsp;</span><a href="http://europeanspallationsource.se/site-civil-works" style="font-size: 14px;" target="_self">the construction site and ongoing civil works</a><span style="color: rgb(50, 51, 51); font-size: 14px;">&nbsp;at the site, including <a href="http://europeanspallationsource.se/site-weekly-updates#aerial" target="_blank">aerial views over time</a>;&nbsp;ESS\'s first-of-its-kind&nbsp;</span><a href="http://europeanspallationsource.se/energy-sustainability" style="font-size: 14px;" target="_self">concept for sustainable energy use</a><span style="color: rgb(50, 51, 51); font-size: 14px;">;&nbsp;and the&nbsp;</span><a href="http://europeanspallationsource.se/licensing-planning" style="font-size: 14px;" target="_self">licensing procedures</a><span style="color: rgb(50, 51, 51); font-size: 14px;">&nbsp;that succesfully cleared the way for the project to break ground in September 2014.</span></div>',
        ),
      ),
    ),
    'field_no_promo' => array(),
    'field_no_slideshow' => array(),
    'group_group' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'language' => 'und',
    'log' => '',
    'path' => array(
      'pathauto' => FALSE,
      'alias' => 'building-ess',
    ),
    'promote' => 0,
    'status' => 1,
    'sticky' => 0,
    'title' => 'Building ESS',
    'tnid' => 0,
    'translate' => 0,
    'type' => 'no_section',
    'uid' => 1,
    'uuid' => '445760a9-392f-55a4-7917-7736dcbc23b0',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'file',
      'uri' => 'file/ed42cab3-5e5e-7e74-3579-87f02a3c05ca',
      'cause' => 'node/99b463d3-c88a-ba64-adcb-8f411d127feb',
    ),
    'field_no_caption' => array(),
    'filemime' => 'image/jpeg',
    'filename' => 'careers_banner1000.jpg',
    'filesize' => 77568,
    'status' => 1,
    'type' => 'image',
    'uid' => '97a7455b-3967-9134-51f6-845ec332b533',
    'uri' => 'public://careers_banner1000.jpg',
    'uuid' => 'ed42cab3-5e5e-7e74-3579-87f02a3c05ca',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'node',
      'uri' => 'node/99b463d3-c88a-ba64-adcb-8f411d127feb',
      'cause' => FALSE,
    ),
    'comment' => 0,
    'field_no_image' => array(
      'und' => array(
        0 => array(
          'alt' => NULL,
          'fid' => 'ed42cab3-5e5e-7e74-3579-87f02a3c05ca',
          'field_no_caption' => array(),
          'filemime' => 'image/jpeg',
          'filename' => 'careers_banner1000.jpg',
          'filesize' => 77568,
          'height' => 266,
          'status' => 1,
          'title' => NULL,
          'type' => 'image',
          'uid' => '97a7455b-3967-9134-51f6-845ec332b533',
          'uri' => 'public://careers_banner1000.jpg',
          'uuid' => 'ed42cab3-5e5e-7e74-3579-87f02a3c05ca',
          'width' => 1000,
        ),
      ),
    ),
    'field_no_lead' => array(
      'und' => array(
        0 => array(
          'format' => 'no_text_editor',
          'value' => '<p>The European Spallation Source, a partnership of European countries, is hiring motivated and inspired people to plan, design, and construct the world&rsquo;s most powerful neutron source. As one of the largest active infrastructure projects in Europe, our team attracts leaders in science and engineering from all over the world. We seek ambitious, talented people who are excited about playing a part in the future of science in Europe. Come and join our adventure.</p>',
        ),
      ),
    ),
    'field_no_promo' => array(),
    'field_no_slideshow' => array(),
    'group_group' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'language' => 'und',
    'log' => '',
    'path' => array(
      'pathauto' => FALSE,
      'alias' => 'careers',
    ),
    'promote' => 0,
    'status' => 1,
    'sticky' => 0,
    'title' => 'Careers',
    'tnid' => 0,
    'translate' => 0,
    'type' => 'no_section',
    'uid' => 1,
    'uuid' => '99b463d3-c88a-ba64-adcb-8f411d127feb',
  );
  $entities['section_nodes'][] = (object) array(
    '__metadata' => array(
      'type' => 'node',
      'uri' => 'node/5def5c10-742c-64f4-b1bb-b0c47a6ed9ae',
      'cause' => FALSE,
    ),
    'body' => array(
      'und' => array(
        0 => array(
          'format' => 'full_html',
          'summary' => '',
          'value' => '<p><iframe frameborder="0" height="1000px" id="ReachMee_area" name="ReachMee_area" scrolling="auto" src="https://web1.reachmee.com/i003/ess/eu/vacancies.aspx" style="margin: -20px 0 0 -40px" width="660px"></iframe></p>',
        ),
      ),
    ),
    'comment' => 0,
    'field_no_attachment' => array(),
    'field_no_lead' => array(),
    'field_no_media' => array(),
    'field_no_section' => array(
      'und' => array(
        0 => array(
          'target_id' => '99b463d3-c88a-ba64-adcb-8f411d127feb',
        ),
      ),
    ),
    'language' => 'und',
    'log' => '',
    'path' => array(
      'pathauto' => FALSE,
      'alias' => 'vacancies',
    ),
    'promote' => 0,
    'status' => 1,
    'sticky' => 0,
    'title' => 'Vacancies',
    'tnid' => 0,
    'translate' => 0,
    'type' => 'no_page',
    'uid' => 1,
    'uuid' => '5def5c10-742c-64f4-b1bb-b0c47a6ed9ae',
  );

  return $entities;
}
