<?php

/**
 * Implements hook_js_alter().
 */
function ess_js_alter(&$javascript) {
  // Unset contrib module javascripts.
  unset($javascript['profiles/esspub/modules/contrib/panels/js/panels.js']);
  unset($javascript['sites/all/modules/panels/js/panels']);
}

/**
 * Change the default meta content-type tag to the shorter HTML5 version.
 */
function ess_html_head_alter(&$head_elements) {
  // Unset meta variables.
  unset($head_elements['system_meta_generator']);
    foreach ($head_elements as $key => $element) {
    if (isset($element['#attributes']['rel']) && $element['#attributes']['rel'] == 'shortlink') {
      unset($head_elements[$key]);
    }
    $head_elements['system_meta_content_type']['#attributes'] = array(
      'charset' => 'utf-8',
    );
  }
}

/**
 * Process variables for the html tag.
 */
function ess_process_html_tag(&$vars) {
  $tag = &$vars['element'];
  if ($tag['#tag'] == 'style' || $tag['#tag'] == 'script') {
    // Remove redundant type attribute and CDATA comments.
    unset($tag['#attributes']['type'], $tag['#value_prefix'], $tag['#value_suffix']);

    // Remove media="all" but leave others unaffected.
    if (isset($tag['#attributes']['media']) && $tag['#attributes']['media'] === 'all') {
      unset($tag['#attributes']['media']);
    }
  }
}

/**
 * Process variables for html.
 */
function ess_preprocess_html(&$vars) {
  global $language;

  unset($vars['classes_array']);
  $vars['classes_array'][] = $vars['is_front'] ? 'front' : 'not-front';
  $vars['classes_array'][] = $vars['logged_in'] ? 'logged-in' : 'not-logged-in';

  if (!$vars['is_front']) {
    // Add unique class for each page.
    $path = drupal_get_path_alias($_GET['q']);
    // Add unique class for each website section.
    list($section, ) = explode('/', $path, 2);
    $arg = explode('/', $_GET['q']);
    $vars['classes_array'][] = drupal_html_class($section);
  }

  // Attributes for html element.
  $vars['html_attributes'] = 'lang="' . $language->language . '" dir="' . $language->dir . '"';
}

/**
 * Add meta tags.
 */
function ess_page_alter($page) {
  $meta_author = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'author',
      'content' => 'ESS - European Spallation Source'
    )
  );
  $meta_description = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'description',
      'content' => 'The European Spallation Source, ESS, will be a state of the art super-microscope generating new science and innovations for a sustainable society.'
    )
  );
  drupal_add_html_head( $meta_author, 'meta_author' );
  drupal_add_html_head( $meta_description, 'meta_description' );
}

/**
 * Changes the search form to use the HTML5 "search" input attribute.
 */
function ess_preprocess_search_block_form(&$vars) {
  $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
}

/**
 * Modify search_form.
 */
function ess_form_search_form_alter(&$form){
  unset($form['basic']['#attributes']['class']['0']);
  $form['basic']['#attributes']['class']['0'] = 'clearfix';
  $form['basic']['keys']['#title_display'] = 'invisible';
  $form['basic']['keys']['#title'] = NULL;
  $form['basic']['keys']['#size'] = '70';
  $form['basic']['#theme_wrappers'] = NULL;
  unset($form['advanced']);
}

/**
 * Modify search_block_form.
 */
function ess_form_search_block_form_alter(&$form) {
  $form['actions']['#theme_wrappers'] = NULL;
  $form['search_block_form']['#theme_wrappers'] = NULL;
	$form['search_block_form']['#id'] = 'edit-search';
  $form['search_block_form']['#title'] = '';
  $form['search_block_form']['#size'] = 25;
  $form_default = t('Search');
  $form['search_block_form']['#default_value'] = $form_default;
  $form['search_block_form']['#attributes'] = array('onblur' => "if (this.value == '') {this.value = '{$form_default}';}", 'onfocus' => "if (this.value == '{$form_default}') {this.value = '';}" );
}

/**
 * Modify the Panels default output to remove the panel separator.
 */
function ess_panels_default_style_render_region($vars) {
  $output = '';
  $output .= implode($vars['panes']);
  return $output;
}

/**
 * Implementation of template_preprocess_panels_pane()
 */
function ess_preprocess_panels_pane(&$vars) {
  // Only use the pane type as a class.
  unset($vars['classes_array']);
  if (isset($vars['pane']->css['css_class'])) {
    $vars['classes_array'][] = $vars['pane']->css['css_class'];
  }
  else
  $vars['classes_array'] = array(
    strtr($vars['pane']->subtype, '_', '-'),
  );
}

/**
 * Implementation of template_preprocess_field().
 */
function ess_preprocess_field(&$vars) {
	$element = $vars['element'];

  unset($vars['classes_array']);
	$vars['field_name_css'] = strtr($element['#field_name'], '_', '-');
  $vars['classes_array'] = array(
    'field',
 		$vars['field_name_css'],
  );

  // Add a "clearfix" class to the wrapper since we float the label and the
  // field items in field.css if the label is inline.
  if ($element['#label_display'] == 'inline') {
    $vars['classes_array'][] = 'clearfix';
  }
}

/**
 * Override of theme_field().
 */
function ess_field($vars) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$vars['label_hidden']) {
    $output .= '<h5' . $vars['title_attributes'] . '>' . $vars['label'] . ':</h5>';
  }

  // Special cases for various fields.
  foreach ($vars['items'] as $delta => $item) {

    // Render the body field.
    if ($vars['element']['#field_name'] == 'body') {
		  $output = '<div class="body">' . drupal_render($item) . '</div>';
		  return $output;
		}

    // Render the kicker field.
    if ($vars['element']['#field_name'] == 'field_no_kicker') {
		  $output = '<div class="kicker">' . drupal_render($item) . '</div>';
		  return $output;
		}

    // Render the lead field.
    if ($vars['element']['#field_name'] == 'field_no_lead') {
		  $output = '<div class="lead">' . drupal_render($item) . '</div>';
		  return $output;
		}

    // Render the image field.
    if ($vars['element']['#field_name'] == 'field_no_media') {
		  $output = drupal_render($item);
		  return $output;
		}

    // Render the media field.
    if ($vars['element']['#field_name'] == 'field_no_image') {
		  $output = '<figure>' . drupal_render($item) . '</figure>';
		  return $output;
		}

    // Render the milestone date field.
    if ($vars['element']['#field_name'] == 'field_no_image') {
		  $output = '<span class="meta">' . drupal_render($item) . '</span>';
		  return $output;
		}
  }

  // Render the items.
  foreach ($vars['items'] as $delta => $item) {
    $classes = 'item';
    $output .= '<div class="' . $classes . '">' . drupal_render($item) . '</div>';
  }
  $output = '<div class="' . $vars['classes'] . '"' . $vars['attributes'] . '>' . $output . '</div>';

  return $output;
}

/**
 * Modify the attachment field.
 */
function ess_field__field_no_attachment($vars) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$vars['label_hidden']) {
    $output .= '<h5' . $vars['title_attributes'] . '>' . $vars['label'] . '</h5>';
  }

  $output .= '<ul>';

  // Render the items.
  foreach ($vars['items'] as $delta => $item) {
    $output .= '<li>' . drupal_render($item) . '</li>';
  }
  $output = '<div class="downloads">' . $output . '</div>';
  $output .= '</ul>';

  return $output;
}

/**
 * Implementation of template_preprocess_node()
 */
function ess_preprocess_node(&$vars) {
  if (isset($vars['content']['links'])) {
    $vars['content']['links']['#attributes']['class'][] = 'clearfix';
  }

  // Only use the a node class and the node type as a class.
  unset($vars['classes_array']);
  $vars['classes_array'] = array(
    'node',
  );

	// Customize the submitted details.
	$vars['submitted'] = date("M j, Y", $vars['created']
  );
}

/**
 * Implementation of template_preprocess_views_view()
 */
function ess_preprocess_views_view(&$vars) {
  global $base_path;

  $view = $vars['view'];

  $vars['rows']       = (!empty($view->result) || $view->style_plugin->even_empty()) ? $view->style_plugin->render($view->result) : '';

  $vars['css_name']   = drupal_clean_css_identifier($view->name);
  $vars['name']       = $view->name;
  $vars['display_id'] = $view->current_display;

  unset($vars['classes_array']);
  $vars['classes_array'] = array(
  );
	if (isset($vars['css_class'])) {
		$vars['classes_array'][] = $vars['css_class'];
	}
	else {
		$vars['classes_array'][] = $vars['css_name'];
	}
}

/**
 * Implementation of template_preprocess_views_view_unformatted()
 */
function ess_preprocess_views_view_unformatted(&$vars) {
  $view = $vars['view'];
  $rows = $vars['rows'];

  $vars['classes_array'] = array();
  $vars['classes'] = array();

  // Set up striping values.
  $count = 0;
  $max = count($rows);

  foreach ($rows as $id => $row) {
    $count++;
    $vars['classes'][$id][] = 'item';

    if ($row_class = $view->style_plugin->get_row_class($id)) {
      $vars['classes'][$id][] = $row_class;
    }

    // Flatten the classes to a string for each row for the template file.
    $vars['classes_array'][$id] = implode(' ', $vars['classes'][$id]);
  }
}

/**
 * Implementation of preprocess_views_view_list().
 */
function ess_preprocess_views_view_list(&$vars) {
  ess_preprocess_views_view_unformatted($vars);
}

/**
 * Override of theme_menu_tree().
 */
function ess_menu_tree($variables) {
  return '<ul class="clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Override of theme_filter_guidelines().
 */
function ess_filter_tips() {
  return '';
}

function ess_filter_tips_more_info() {
  return '';
}

/**
 * Override of theme_menu_local_tasks().
 */
function ess_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<ul class="tabs clearfix">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<ul class="tabs clearfix">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
 * Process variables for menu-block-wrapper.tpl.php.
 */
function ess_preprocess_menu_block_wrapper(&$vars) {
  unset($vars['classes_array']);
  $vars['classes_array'][] = $vars['config']['menu_name'];
}

/**
 * Override of theme_menu_link().
 */
function ess_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  $mlid_class = 'menu-' . $element['#original_link']['mlid'];
  $element['#attributes']['class'] = '';
  $element['#attributes']['class'] .= $mlid_class;
  $custom_class =  str_replace(' ','-',strtolower($element['#title']));

  if ($element['#original_link']['menu_name'] == 'menu-social') {
    $element['#attributes']['class'] .= $custom_class;
  }

  $element['#localized_options']['html'] = TRUE;

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  // this was disabled as part of fixing down-arrows on menu items
  /*if ($element['#original_link']['has_children'] && $element['#below']) {
    $element['#attributes']['class'][] = 'expanded';
    }*/

  // Add span tags, but only to the main menu.
  if ($element['#original_link']['menu_name'] == 'main-menu') {
    $output = l('<span>' . check_plain($element['#title']) . '</span>', $element['#href'], $element['#localized_options']);
  } else {
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  }

  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Override of theme_file_link().
 */
function ess_file_link($variables) {
  $file = $variables['file'];
  $icon_directory = $variables['icon_directory'];

  $path = pathinfo($file->uri);
  $extension = $path['extension'];

  $mime = $file->filemime;
  $size = format_size($file->filesize);
  $type= $file->type;

  $url = file_create_url($file->uri);
  $icon = theme('file_icon', array('file' => $file, 'icon_directory' => $icon_directory));

  $options = array(
    'attributes' => array(
      'class' => $extension,
      'type' => $file->filemime . '; length=' . $file->filesize,
    ),
  );

  if (empty($file->description)) {
    $link_text = $file->filename;
  }
  else {
    $link_text = $file->description;
    $options['attributes']['title'] = check_plain($file->filename);
  }

  // Set target _blank on all files.
  $options['attributes']['target'] = '_blank';

  if (drupal_strlen($link_text) > 70) {
    $link_text = drupal_substr($link_text, 0, 65) . '...';
  }
  return l($link_text, $url, $options) . '<small>' . $size . ' ' . $extension . '</small>';
}

/**
 * Change titles for user pages.
 */
function ess_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'user_register_form') {
    $title = drupal_set_title('Create new account');
  }
  elseif ($form_id == 'user_pass') {
    $title = drupal_set_title('Request new password');
  }
  elseif ($form_id == 'user_login') {
    drupal_set_title('Log in');
  }
}

/**
 * Override of theme_form().
 */
function ess_form($variables) {
  $element = $variables['element'];
  if (isset($element['#action'])) {
    $element['#attributes']['action'] = drupal_strip_dangerous_protocols($element['#action']);
  }
  element_set_attributes($element, array('method', 'id'));
  if (empty($element['#attributes']['accept-charset'])) {
    $element['#attributes']['accept-charset'] = "UTF-8";
  }
  // Anonymous DIV to satisfy XHTML compliance.
  return '<form' . drupal_attributes($element['#attributes']) . '><div class="clearfix">' . $element['#children'] . '</div></form>';
}

/**
 * Override of theme_form_element().
 */
function ess_form_element($variables) {
  $element = &$variables['element'];

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');

  // Add a class for disabled elements to facilitate cross-browser styling.

  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  if (!empty($element['#description'])) {
    $output .= '<small>' . $element['#description'] . "</small>\n";
  }

  $output .= "</div>\n";

  return $output;
}

/**
 * Override of theme_textarea().
 */
function ess_textarea($variables) {
  // Remove resizable textareas.
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'cols', 'rows'));
  _form_set_class($element, array('form-textarea'));

  $output = '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
  return $output;
}

/**
 * Override of theme_date_display_single().
 */
function ess_date_display_single($variables) {
  $date = $variables['date'];
  $timezone = $variables['timezone'];
  $attributes = $variables['attributes'];

  return drupal_attributes($attributes) . $date . $timezone;
}
