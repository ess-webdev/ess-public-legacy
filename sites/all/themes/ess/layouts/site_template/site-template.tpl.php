<?php if (!empty($content['top'])): ?>
  <div class="topbar clearfix">
    <?php print render($content['top']); ?>
  </div>
<?php endif; ?>

<?php if (!empty($content['header'])): ?>
  <header id="header">
    <div class="wrap clearfix">
      <?php print render($content['header']); ?>
    </div>
  </header>
<?php endif; ?>

<?php if (!empty($content['navigation'])): ?>
  <div id="navigation">
    <div class="wrap clearfix">
      <?php print render($content['navigation']); ?>
    </div>
  </div>
<?php endif; ?>

<?php if (!empty($content['highlight'])): ?>
  <div id="highlight">
    <?php print render($content['highlight']); ?>
  </div>
<?php endif; ?>

<?php if (!empty($content['content'])): ?>
  <div id="content">
    <div class="wrap clearfix">
      <?php print render($content['content']); ?>
    </div>
  </div>
<?php endif; ?>

<?php if (!empty($content['footer'])): ?>
  <footer id="footer">
    <div class="footer-links">
      <div class="wrap clearfix">
        <?php print render($content['footer']); ?>
      </div>
    </div>
    <?php if (!empty($content['footer_closure'])): ?>
      <div class="footer-closure">
        <div class="wrap clearfix">
          <?php print render($content['footer_closure']); ?>
        </div>
      </div>
    <?php endif; ?>
  </footer>
<?php endif; ?>