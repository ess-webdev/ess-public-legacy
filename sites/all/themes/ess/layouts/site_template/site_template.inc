<?php
/**
 * @file
 * Definition of the site template layout.
 */
$plugin = array(
  'title' => t('Site template'),
  'theme' => 'site_template',
  'icon' => 'site_template.png',
  'category' => 'Builders',
  'regions' => array(
    'top' => t('Top'),
    'header' => t('Header'),
    'navigation' => t('Navigation'),
    'highlight' => t('Highlight'),
    'content' => t('Content'),
    'footer' => t('Footer'),
    'footer_closure' => t('Footer closure'),
  ),
);
