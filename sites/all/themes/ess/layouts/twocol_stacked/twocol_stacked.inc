<?php
$plugin = array(
  'title' => t('Two column stacked'),
  'category' => 'Columns: 2',
  'icon' => 'twocol_stacked.png',
  'theme' => 'twocol_stacked',
  'regions' => array(
    'left' => t('Left'),
    'right' => t('Right'),
  ),
);
