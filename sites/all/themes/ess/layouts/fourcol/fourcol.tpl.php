<?php if (!empty($content['column_one'])): ?>
  <section>
    <?php print render($content['column_one']); ?>
  </section>
<?php endif; ?>

<?php if (!empty($content['column_two'])): ?>
  <section>
    <?php print render($content['column_two']); ?>
  </section>
<?php endif; ?>

<?php if (!empty($content['column_three'])): ?>
  <section>
    <?php print render($content['column_three']); ?>
  </section>
<?php endif; ?>

<?php if (!empty($content['column_four'])): ?>
  <section>
    <?php print render($content['column_four']); ?>
  </section>
<?php endif; ?>
