<?php
$plugin = array(
  'title' => t('Four columns'),
  'category' => 'Columns: 4',
  'icon' => 'fourcol.png',
  'theme' => 'fourcol',
  'regions' => array(
    'column_one' => t('Column one'),
    'column_two' => t('Column two'),
    'column_three' => t('Column three'),
    'column_four' => t('Column four'),
  ),
);
