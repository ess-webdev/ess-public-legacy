<?php if (!empty($content['main'])): ?>
  <section id="main">
    <?php print render($content['main']); ?>
  </section>
<?php endif; ?>
