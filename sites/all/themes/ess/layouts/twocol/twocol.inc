<?php
$plugin = array(
  'title' => t('Two columns'),
  'category' => 'Columns: 2',
  'icon' => 'twocol.png',
  'theme' => 'twocol',
  'regions' => array(
    'header' => t('Header'),
    'highlight' => t('Highlight'),
    'main' => t('Main'),
    'aside' => t('Aside'),
    'footer' => t('Footer'),
  ),
);
