<?php if (!empty($content['header'])): ?>
  <header role="page">
    <?php print render($content['header']); ?>
  </header>
<?php endif; ?>

<?php if (!empty($content['main'])): ?>
  <section id="main">
    <?php print render($content['main']); ?>
  </section>
<?php endif; ?>

<?php if (!empty($content['aside'])): ?>
  <aside role="sub">
    <?php print render($content['aside']); ?>
  </aside>
<?php endif; ?>

<?php if (!empty($content['footer'])): ?>
  <footer role="page">
    <?php print render($content['footer']); ?>
  </footer>
<?php endif; ?>