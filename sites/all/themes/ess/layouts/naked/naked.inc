<?php
$plugin = array(
  'title' => t('Naked'),
  'category' => 'Columns: 1',
  'icon' => 'naked.png',
  'theme' => 'naked',
  'regions' => array(
    'main' => t('Main'),
  ),
);
