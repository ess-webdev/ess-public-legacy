<?php
/**
 * @file
 * Default theme implementation to wrap menu blocks.
 */
?>
<nav class="<?php print ($classes); ?>">
  <?php print render($content); ?>
</nav>

