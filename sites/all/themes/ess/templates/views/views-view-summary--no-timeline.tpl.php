<?php
/**
 * @file views-view-summary.tpl.php
 * Default simple view template to display a list of summary lines
 *
 * @ingroup views_templates
 */
?>
<ul class="clearfix">
<?php $first = 'class="active"'; ?>
<?php foreach ($rows as $id => $row): ?>
  <li class="<?php print $row->link; ?>"><a <?php print $first; ?> href="#"><?php print $row->link; ?></a></li>
  <?php if ($first) { $first = ''; } ?>
<?php endforeach; ?>
</ul>
