<?php
/**
* @file
* Theme the more link.
*/
?>
<a class="more-link" href="<?php print $more_url ?>">
  <?php print $link_text; ?>
</a>
