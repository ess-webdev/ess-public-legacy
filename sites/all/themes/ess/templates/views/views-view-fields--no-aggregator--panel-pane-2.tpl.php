<?php foreach ($fields as $id => $field): ?>

  <?php
    $content = $field->content;
    $content = check_markup($content, 'full_html');
    $content = str_replace('ESS_Scandinavia: ', '', $content);
    $content = preg_replace("(@([a-zA-Z0-9\_]+))", "<a href=\"http://twitter.com/\\1\">\\0</a>", $content);
    $content = preg_replace('/(^|\s)#(\w*[a-zA-Z_]+\w*)/', '\1<a href="http://twitter.com/search/%23\2">#\2</a>', $content);
  ?>

  <?php print $content; ?>
<?php endforeach; ?>
