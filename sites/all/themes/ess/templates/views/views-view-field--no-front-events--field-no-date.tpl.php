<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>
<?php
  $start = strtotime($row->field_field_no_date[0]['raw']['value']." UTC");
  $end = strtotime($row->field_field_no_date[0]['raw']['value2']." UTC");
  $compare_start = date('Y-m-d', $start);
  $compare_end = date('Y-m-d', $end);

  if ($compare_start == $compare_end) {
    $output = date('l, F j, Y H:i', $start) . ' - ' . date('H:i', $end);
  }
  else {
    $output = date('l, F j, Y', $start) . ' - ' . date('l, F j, Y', $end);
  }

  print $output;
?>
