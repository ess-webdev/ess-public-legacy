<?php
/**
 * @file
 *
 * Theme implementation to display the messages area, which is normally
 * included roughly in the content area of a page.
 */
 ?>
<?php print $messages; ?>
<?php print $help; ?>
<?php if ($tabs): ?>
  <?php print render($tabs); ?>
<?php endif; ?>
<?php if ($action_links): ?>
  <ul class="action-links">
    <?php print render($action_links); ?>
  </ul>
<?php endif; ?>
