<?php
/**
 * @file
 * Default theme implementation for displaying a single search result.
 */
?>
<li>
  <h5>
    <a href="<?php print $url; ?>"><?php print $title; ?></a>
  </h5>
  <cite><?php print $url; ?></cite>
  <?php if ($snippet): ?>
    <div class="snippet"><?php print $content_attributes; ?><?php print $snippet; ?></div>
  <?php endif; ?>
</li>
