<!DOCTYPE html>
<html <?php print $html_attributes; ?> xmlns:og="http://opengraphprotocol.org/schema/">
<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?>
<?php print $scripts; ?>
<!--[if lt IE 9]>
<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body class="<?php print $classes; ?>">
<?php if( extension_loaded('newrelic') ) { echo newrelic_get_browser_timing_header(); } ?>
<?php print $page_top; ?>
<?php print $page; ?>
<?php print $page_bottom; ?>
<?php if( extension_loaded('newrelic') ) { echo newrelic_get_browser_timing_footer(); } ?>
</body>
