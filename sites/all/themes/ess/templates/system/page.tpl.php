<header id="header">
  <div class="wrap clearfix">
    <?php if ($site_name): ?>
    	<h1><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h1>   
    <?php endif; ?>
  </div>
</header>
      
<div id="content">
  <div class="wrap clearfix">
	  <?php print $messages; ?>
    <?php print render($tabs); ?>
    <?php print render($page['help']); ?>
		<h1><?php print $title; ?></h1>
    <?php if ($action_links): ?>
      <ul class="action-links clearfix"><?php print render($action_links); ?></ul>
    <?php endif; ?>
    <?php print render($page['content']); ?>
  </div>
</div>
  
<footer id="footer">
  <div class="wrap clearfix">
    <?php print render($page['footer']); ?>
  </div>
</footer>
