<?php
/**
 * @file
 * Theme implementation to display the header block on a Drupal page.
 */
?>
<?php if ($site_name): ?>
	<h1><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h1>   
<?php endif; ?>
<?php print render($page['header']); ?>
