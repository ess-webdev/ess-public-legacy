<figure class="<?php print $type ?>">

  <?php
    hide($content['field_no_caption']);
    print render($content);
    print render($field_no_caption);
  ?>

  <?php if (!$page): ?>
    <?php if (!empty($file->field_no_caption)): ?>
      <figcaption><?php print check_plain($file->field_no_caption[LANGUAGE_NONE][0]['value']); ?></figcaption>
    <?php endif; ?>
  <?php endif; ?>
  
</figure>
