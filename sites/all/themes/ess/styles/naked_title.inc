<?php
/**
 * @file
 * Definition of the Naked with title panel style.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Naked with title'),
  'description' => t('Display the pane with no markup, except the title.'),
  'render pane' => 'naked_title_style_render_pane',
  'pane settings form' => 'naked_title_style_settings_form',
);

/**
 * Render callback.
 */
function theme_naked_title_style_render_pane($vars) {
  $content = $vars['content'];
  $output = '';

  if (!empty($content->title)) {
    $output .= '<h3>' . $content->title . '</h3>';
  }
  
  $output .= render($vars['content']->content);

  return $output;
}
