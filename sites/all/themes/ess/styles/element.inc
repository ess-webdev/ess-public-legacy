<?php
/**
 * @file
 * Definition of the element panel style.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Element'),
  'description' => t('Wrap content in a HTML element.'),
  'render pane' => 'element_render_pane',
  'render region' => 'element_render_region',
  'settings form' => 'element_settings_form',
  'pane settings form' => 'element_pane_settings_form',
);

/**
 * Settings form callback.
 */
function element_settings_form($settings) {
  $form['content_tag'] = array(
    '#type' => 'select',
    '#title' => t('HTML element'),
    '#options' => array(
      'div' => 'Div',
      'section' => 'Section',
      'article' => 'Article',
      'aside' => 'Aside',
      'header' => 'Header',
      'footer' => 'Footer',
    ),
  );
  $form['content_id'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS ID'),
    '#default_value' => (isset($settings['content_id'])) ? $settings['content_id'] : '',
  );
  $form['content_class'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS class'),
    '#default_value' => (isset($settings['content_class'])) ? $settings['content_class'] : '',
  );

  return $form;
}

/**
 * Pane settings form callback.
 */
function element_pane_settings_form($settings) {
  $form['content_tag'] = array(
    '#type' => 'select',
    '#title' => t('HTML element'),
    '#options' => array(
      'div' => 'Div',
      'section' => 'Section',
      'article' => 'Article',
      'aside' => 'Aside',
      'header' => 'Header',
      'footer' => 'Footer',
    ),
  );
  $form['content_id'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS ID'),
    '#default_value' => (isset($settings['content_id'])) ? $settings['content_id'] : '',
  );
  $form['content_class'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS class'),
    '#default_value' => (isset($settings['content_class'])) ? $settings['content_class'] : '',
  );

  return $form;
}

/**
 * Theme function for the pane style.
 */
function theme_element_render_pane($vars) {
  $content = $vars['content'];
  $settings = $vars['settings'];
  $output = '';

  if (empty($settings['content_tag'])) {
    $settings['content_tag'] = 'div';
  }
  
  if (empty($settings['content_id'])) {
    $settings['content_id'] = '';
  }

  if (empty($settings['content_class'])) {
    $settings['content_class'] = '';
  }

  $output = '<' . $settings['content_tag'];
  $output .= !empty($settings['content_id']) ? ' id="' . $settings['content_id'] . '"' : '';
  $output .= !empty($settings['content_class']) ? ' class="' . $settings['content_class'] . '">' : '>';

  $output .= render($content->content) . '</' . $settings['content_tag'] . '>';
  
  return $output;
}

/**
 * Theme function for the region style.
 */
function theme_element_render_region($vars) {
  $settings = $vars['settings'];
  $panes = $vars['panes'];
  
  if (empty($settings['content_tag'])) {
    $settings['content_tag'] = 'div';
  }
  
  if (empty($settings['content_id'])) {
    $settings['content_id'] = '';
  }

  if (empty($settings['content_class'])) {
    $settings['content_class'] = '';
  }

  $output = '<' . $settings['content_tag'];
  $output .= !empty($settings['content_id']) ? ' id="' . $settings['content_id'] . '"' : '';
  $output .= !empty($settings['content_class']) ? ' class="' . $settings['content_class'] . '">' : '>';
  $output .= implode("\n", $panes) . '</' . $settings['content_tag'] . '>';

  return $output;
}