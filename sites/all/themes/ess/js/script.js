(function($) {

  // Open external links in new window.
  Drupal.behaviors.external = {
    attach : function (context, settings) {
      $('a:not([href=""])').each(function() {
        if (this.hostname !== location.hostname) {
          $(this).addClass('external').attr('target', "_blank");
        }
      });
    }
  }

  // Superfish menu.
  Drupal.behaviors.superfish = {
    attach : function(context, settings) {
      $('#navigation ul').superfish({
        delay: 100,
        hoverClass: 'hover',
        pathClass: 'active',
        animation: {opacity:'show'},
        speed: 'fast',
        autoArrows: false,
        dropShadows: false
      });
    }
  }

  // Slideshow.
  Drupal.behaviors.slideshow = {
    attach : function(context, settings) {
      attachSlideshow($('.slideshow', context));
      function attachSlideshow(field) {
        if ($('li', field).length > 1) {
          field.append('<ul class="slide-pagination"></ul>');
          field.append('<ul class="slide-navigation"><li class="prev"><a href="#">Prev</a></li><li class="next"><a href="#">Next</a></li></ul>');
          $('ul.slides', field).cycle({
            fx: 'scrollHorz',
            speed: 1100,
            easing: 'easeOutQuint',
            sync: 1,
            next: '.slide-navigation .next',
            prev: '.slide-navigation .prev',
            activePagerClass: 'active',
            pause: 1,
            pauseOnPagerHover: 1,
            pager: '.slide-pagination',
            pagerAnchorBuilder: function(idx, slide) {
              var title = $('.slideshow').find("h1").map(function() { return $(this).text(); });
              return '<li><a href="#">'+ title[idx] + '</a></li>';
            },
            timeout: 6000
          });
        }
      }
    }
  }

  $(window).load(function() {

    // Loop through different article regions
    $('.no-articles').each(function() {
      // Set a boolean to default false
      var has_image = false;
      // Switch for every different ID for all sections
      switch (this.id) {
        // When ID matches 'news'
        case 'news':
          var current = $('#news .articles .item');
          // Loop throught every item in the current article section
          current.each(function() {
            // If one of the items in the section has an image, set this region-variable as true.
            if ($(this).find("img").length > 0) {
              has_image = true;
            }
          });
          break;

        // When ID matches 'partners'
        case 'partners':
          var current = $('#partners .articles .item');
          // Loop throught every item in the current article section
          current.each(function() {
            // If one of the items in the section has an image, set this region-variable as true.
            if ($(this).find("img").length > 0) {
              has_image = true;
            }
          });
          break;

        // When ID matches 'science'
        case 'science':
          var current = $('#science .articles .item');
          // Loop throught every item in the current article section
          current.each(function() {
            // If one of the items in the section has an image, set this region-variable as true.
            if ($(this).find("img").length > 0) {
              has_image = true;
            }
          });
          break;

      }

      // If the section has an image
      if(has_image) {
        // Loop through every item in section
        $(current, '.item').each(function() {
          // If the item has an image
          if (!$(this).find("img").length > 0) {
            // Set a 'no-image' class to the item
            $(this).addClass('no-image');
          }
        })
      }
    });

    // Wrap inner
    $( ".main-menu > ul > li" ).wrapInner('<div class="nav-item-wrapper" />');

    // Equal height for partners titles
    var $box = $('#partners .item h2');
    var boxHeight = 0;

    $box.each(function() {
      boxHeight = Math.max($(this).height(), boxHeight);
    });
    $box.height(boxHeight);

    // Equal height for partners paragraph
    var $box = $('#partners .item p:not(.more)');
    var boxHeight = 0;

    $box.each(function() {
      boxHeight = Math.max($(this).height(), boxHeight);
    });
    $box.height(boxHeight);

    // Equal height for science titles
    var $box = $('#science .item h2');
    var boxHeight = 0;

    $box.each(function() {
      boxHeight = Math.max($(this).height(), boxHeight);
    });
    $box.height(boxHeight);

    // Equal height for science paragraph
    var $box = $('#science .item p:not(.more)');
    var boxHeight = 0;

    $box.each(function() {
      boxHeight = Math.max($(this).height(), boxHeight);
    });
    $box.height(boxHeight);

    // Equal height for news
    var $box = $('#news .item h2');
    var boxHeight = 0;

    $box.each(function() {
      boxHeight = Math.max($(this).height(), boxHeight);
    });
    $box.height(boxHeight);

    // Equal height for news paragraph
    var $box = $('#news .item p:not(.more)');
    var boxHeight = 0;

    $box.each(function() {
      boxHeight = Math.max($(this).height(), boxHeight);
    });
    $box.height(boxHeight);

    // If top-right is bigger than feature article, make them the same height.
    if($('.front .top-right').height() > $('.front .featured-article').height()) {
      $('.front .featured-article .image').height($('.front .top-right').height() -3);
      $('.front .featured-article .image').addClass('big');
    }
    // If above statement is false, make featured article image the same height as the featured article text.
    else {
      $('.front .featured-article .image').height($('.featured-article .item').height());
      $('.front .take-note-area').height($('.featured-article .item').height() -25);
    }

    // Equal heights for related articles.
    var $frontBlocks = $('.related a');
        $frontBlocks.each(function() {
          frontBlockHeight = Math.max($(this).height(), frontBlockHeight);
      });
    $frontBlocks.height(frontBlockHeight);
    frontBlockHeight = 0;

    // Equal heights for related articles.
    var $frontBlocks = $('.related p:not(.meta)');
        $frontBlocks.each(function() {
          frontBlockHeight = Math.max($(this).height(), frontBlockHeight);
      });
    $frontBlocks.height(frontBlockHeight);
    frontBlockHeight = 0;
  });

})(jQuery);
